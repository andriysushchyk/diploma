<?php

namespace Tests;

use App\Models\School;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();
        $db = app()->make('db');
        $db->getSchemaBuilder()->enableForeignKeyConstraints();
    }

    /**
     * @param School $school
     * @return TestCase
     */
    public function signInAsSchoolManager(School $school)
    {
        $user = create(User::class);
        $user->schools()->attach([
            $school->id => [
                'permissions' => 1
            ]
        ]);

        return $this->actingAs($user);
    }

    public function signIn($userOrSchool = null)
    {
        if ($userOrSchool instanceof School) {
            $user = create(User::class);
            $user->schools()->sync([$userOrSchool->id]);

        }   else {
            $user = $userOrSchool;
        }
        return $this->actingAs($user);
    }
}
