<?php

namespace Tests\Feature\School;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatePasswordTest extends TestCase
{
    use RefreshDatabase;

    protected $oldPassword = '123456';

    protected $newPassword = '12345678';

    /** @var User */
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = create(User::class, [
            'password' => bcrypt($this->oldPassword)
        ]);
    }

    /** @test */
    public function it_updates_user_password()
    {
        $this
            ->actingAs($this->user)
            ->putJson('/school/api/profile/password', [
                'password' => $this->newPassword,
                'password_confirmation' => $this->newPassword,
                'old_password' => $this->oldPassword
            ])
            ->assertStatus(200);

        $this->user->refresh();
        $this->assertTrue(password_verify($this->newPassword, $this->user->password));
    }

    /** @test */
    public function it_returns_error_when_old_password_is_wrong()
    {
        $wrongPassword = $this->oldPassword . " ";
        $this
            ->actingAs($this->user)
            ->putJson('/school/api/profile/password', [
                'password' => $this->newPassword,
                'password_confirmation' => $this->newPassword,
                'old_password' => $wrongPassword
            ])
            ->assertStatus(422);

        $this->user->refresh();
        $this->assertFalse(password_verify($this->newPassword, $wrongPassword));
    }
}
