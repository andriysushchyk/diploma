<?php

namespace Tests\Feature\School;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\LessonType;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageLessonsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_stores_additional_lesson()
    {
        $user = create(User::class);
        $requestData = [
            'group_subject_id' => create(GroupSubject::class)->id,
            'lesson_type' => 'potochna',
            'date' => '2018-10-10'
        ];

        $this
            ->signIn($user)
            ->postJson('/school/api/lessons', $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('additional_lessons', $requestData);
    }

    public function it_destroys_additional_lesson()
    {
        $requestData = [

        ];
    }
}
