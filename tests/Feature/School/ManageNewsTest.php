<?php

namespace Tests\Feature\Feature\School;

use App\Models\NewsItem;
use App\Models\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageNewsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_news_page()
    {
        $school = create(School::class);
        $this
            ->signIn($school)
            ->get('/school/news')
            ->assertStatus(200);
    }

    /** @test */
    public function it_opens_page_for_creating_news()
    {
        $school = create(School::class);
        $this
            ->signIn($school)
            ->get('/school/news/create')
            ->assertStatus(200);
    }

    /** @test */
    public function fit_shows_news()
    {
        $school = create(School::class);
        $newsItem = create(NewsItem::class, [
            'school_id' => $school->id
        ]);

        $this
            ->signIn($school)
            ->get('/school/news/' . $newsItem->id)
            ->assertStatus(200);
    }

    /** @test */
    public function it_saves_news_item()
    {
        $title = 'asdasdasdasd';
        $school = create(School::class);
        $this
            ->signIn($school)
            ->postJson('/school/api/news', [
                'image' => file_get_contents(base_path('tests/stubs/image_base64')),
                'title' => $title,
                'preview_text' => 'qweqwe',
                'full_text' => 'qweqwe'
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('news', [
            'title' => $title
        ]);
    }

    /** @test */
    public function it_deletes_news()
    {
        $school = create(School::class);
        $newsItem = create(NewsItem::class, [
            'school_id' => $school->id
        ]);
        $this
            ->signIn($school)
            ->deleteJson('/school/api/news/' . $newsItem->id)
            ->assertStatus(200);

        $this->assertDatabaseMissing('news', [
            'id' => $newsItem->id
        ]);

    }
}
