<?php

namespace Tests\Feature\School;

use App\Models\AdditionalLesson;
use App\Models\MarkValue;
use App\Models\TimetableItem;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageMarksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_stores_mark()
    {
        $user = create(User::class);
        $markValue = create(MarkValue::class);
        $additionalLesson = create(AdditionalLesson::class);
        $timetableItem = create(TimetableItem::class);

        $this
            ->signIn($user)
            ->postJson('/school/api/marks', [
                'user_id'       => $user->id,
                'mark_value_id' => $markValue->id,
                'additional_lesson_id' => null,
                'timetable_item_id' => $timetableItem->id,
                'date' => '2012-10-10'
            ])->assertStatus(200);

        $this->assertDatabaseHas('marks', [
            'mark_value_id' => $markValue->id,
            'mark_lesson_id' => $timetableItem->id,
            'mark_lesson_type' => TimetableItem::class
        ]);

        $this
            ->signIn($user)
            ->postJson('/school/api/marks', [
                'user_id'       => $user->id,
                'mark_value_id' => $markValue->id,
                'additional_lesson_id' => $additionalLesson->id,
                'timetable_item_id' => null,
                'date' => '2012-10-10'
            ])->assertStatus(200);

        $this->assertDatabaseHas('marks', [
            'mark_value_id' => $markValue->id,
            'mark_lesson_id' => $additionalLesson->id,
            'mark_lesson_type' => AdditionalLesson::class
        ]);
    }
}
