<?php

namespace Tests\Feature\School;

use App\Models\GroupSubject;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHomeworkTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_updates_homework()
    {
        $user = create(User::class);
        $groupSubject = create(GroupSubject::class);
        $work = 'asdsadasdasd';
        $requestData = [
            'group_subject_id' => create(GroupSubject::class)->id,
            'work' => 'asdasdasd',
            'date' => '2018-10-10'
        ];

        $this
            ->signIn($user)
            ->putJson('/school/api/homework', $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('homeworks', $requestData);
    }
}
