<?php

namespace Tests\Feature\School;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OpenJournalTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_journal()
    {
        $user = create(User::class, [
            'role' => Role::TEACHER
        ]);
        $groupSubject = create(GroupSubject::class);

        $this
            ->signIn($user)
            ->get('/school/journal/' . $groupSubject->id)
            ->assertStatus(200);
    }
}
