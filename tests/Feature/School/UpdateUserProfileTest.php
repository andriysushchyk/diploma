<?php

namespace Tests\Feature\School;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateUserProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_updates_user_profile_data()
    {
        $username = 'user22222';

        $user = create(User::class);
        $this
            ->actingAs($user)
            ->putJson('/school/api/profile', [
                'avatar' => file_get_contents(base_path('tests/stubs/image_base64')),
                'login' => $username
            ])
            ->assertStatus(200);

        $user->refresh();
        $this->assertNotEquals('images/default.png', $user->avatar_path);
        $this->assertEquals($username, $user->login);
        $this->assertFileExists(Storage::disk('public')->path($user->avatar_path));
        unlink(Storage::disk('public')->path($user->avatar_path));
    }

    /** @test */
    public function it_opens_profile_editing_page()
    {
        $user = create(User::class);
        $this
            ->actingAs($user)
            ->get('/school/profile')
            ->assertStatus(200);
    }
}
