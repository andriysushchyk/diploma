<?php

namespace Tests\Feature\School;

use App\Models\Group;
use App\Models\School;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OpenDaybookTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_daybook()
    {
        $school = create(School::class);
        $user = create(User::class);
        $user->schools()->sync([$user->id]);
        $group = create(Group::class, [
            'school_id' => $school->id
        ]);
        $group->users()->sync([$user->id]);

        $this
            ->signIn($user)
            ->get('/school/daybook?date=asdasd')
            ->assertStatus(200);

        $this
            ->signIn($user)
            ->get('/school/daybook?date=2020-10-10')
            ->assertStatus(404);
    }
}
