<?php

namespace Tests\Feature\Api\SchoolApplications;

use App\Models\City;
use App\Models\OrganizationType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreSchoolApplicationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function storeSchoolApplication_rightData_shouldSuccess()
    {
        $schoolFullName = $this->faker->words(3, true);

        $requestData = [
            'school_short_name'           => $this->faker->word,
            'school_full_name'            => $schoolFullName,
            'school_phone_number'         => $this->faker->phoneNumber,
            'school_city_id'              => factory(City::class)->create()->id,
            'school_organization_type_id' => factory(OrganizationType::class)->create()->id,
            'sender_first_name'           => $this->faker->word,
            'sender_middle_name'          => $this->faker->word,
            'sender_last_name'            => $this->faker->word,
            'sender_position'             => $this->faker->word,
            'sender_phone_number'         => $this->faker->phoneNumber,
            'sender_email'                => $this->faker->email
        ];

        $this
            ->postJson('/api/school_applications', $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('school_applications', [
            'school_full_name' => $schoolFullName
        ]);
    }
}
