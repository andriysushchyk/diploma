<?php

namespace Tests\Feature\Guest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OpenHomePageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_home_page()
    {
        $this
            ->get('/')
            ->assertStatus(200);
    }
}
