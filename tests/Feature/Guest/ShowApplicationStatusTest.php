<?php

namespace Tests\Feature\Guest;

use App\Models\SchoolApplication;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowApplicationStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_show_application_status()
    {
        $application = create(SchoolApplication::class);
        $this
            ->get('/applications/' . $application->id)
            ->assertStatus(200);
    }
}
