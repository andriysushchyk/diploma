<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function client_should_get_422_error_when_parameters_are_not_valid()
    {
        $this->postJson('/auth/login', [
            'asd'    => 'username',
            'pasasd' => 123
        ])->assertStatus(422);
    }

    /** @test */
    public function client_should_get_422_error_when_credentials_are_not_valid()
    {
        $this->postJson('/auth/login', [
            'login'    => 'username',
            'password' => 'password'
        ])->assertStatus(422);
    }

    /** @test */
    public function client_should_get_200_status_when_credentials_are_valid()
    {
        $login = 'login';
        $password = 'password';

        factory(User::class)->create([
            'login'    => $login,
            'password' => bcrypt($password)
        ]);

        $this->postJson('/auth/login', [
            'login'    => $login,
            'password' => $password
        ])->assertStatus(200);
    }
}
