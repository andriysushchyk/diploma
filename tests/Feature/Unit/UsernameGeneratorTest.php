<?php

namespace Tests\Feature\Unit;

use App\Models\User;
use App\Services\UsernameGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsernameGeneratorTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_generates_correct_user_name()
    {
        $user = create(User::class, [
            'last_name' => "Сущик",
            'first_name' => "Андрій",
            'middle_name' => "Миколайович",
        ]);

        $username = ((new UsernameGenerator())->generate($user));
        $this->assertEquals('sushchyk_andrii_mykolaiovych', $username);
    }
}
