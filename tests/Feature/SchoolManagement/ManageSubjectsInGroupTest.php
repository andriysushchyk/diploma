<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSubjectsInGroupTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_creates_subject_in_group()
    {
        $group = create(Group::class);
        $requestData = [
            'title'      => 'Математика',
            'teacher_id' => create(User::class)->id
        ];

        $this
            ->signInAsSchoolManager($group->school)
            ->postJson('/school_management/api/subjects/group/' . $group->id, $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('groups_subjects', array_merge($requestData, [
            'group_id' => $group->id
        ]));
    }

    /** @test */
    public function it_updates_subject_in_group()
    {
        $requestData = [
            'title'      => 'Математика',
            'teacher_id' => create(User::class)->id,
        ];

        $groupSubject = create(GroupSubject::class);
        $this
            ->signInAsSchoolManager($groupSubject->group->school)
            ->putJson('/school_management/api/subjects/' . $groupSubject->id, $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('groups_subjects', array_merge($requestData, [
            'group_id' => $groupSubject->group_id
        ]));
    }

    /** @test */
    public function it_destroys_subject_in_group()
    {
        $groupSubject = create(GroupSubject::class);
        $this
            ->signInAsSchoolManager($groupSubject->group->school)
            ->deleteJson('/school_management/api/subjects/' . $groupSubject->id)
            ->assertStatus(200);

        $this->assertDatabaseMissing('groups_subjects', [
            'group_id' => $groupSubject->group_id
        ]);
    }
}
