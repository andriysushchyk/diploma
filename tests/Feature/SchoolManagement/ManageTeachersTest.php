<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageTeachersTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function it_lists_school_teachers()
    {
        $school = create(School::class);
        $teachers = create(User::class, ['role' => Role::TEACHER], 5);
        $school->users()->sync($teachers->pluck('id'));
        $this
            ->signInAsSchoolManager($school)
            ->get('school_management/teachers')
            ->assertStatus(200);
    }

    /** @test
     * @throws \Exception
     */
    public function it_stores_teacher()
    {
        $school = create(School::class);

        $requestData = [
            'login' => '',
            'first_name' => $this->faker->firstName,
            'middle_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];

        $result = $this
            ->signInAsSchoolManager($school)
            ->postJson('school_management/api/teachers', $requestData)
            ->assertStatus(200)
            ->assertJsonStructure(['item'])
            ->decodeResponseJson();

        $this->assertNotEmpty($result['item']['id']);
        $this->assertDatabaseHas('users', [
            'first_name' => $requestData['first_name'],
            'role' => Role::TEACHER
        ]);

        $this->assertDatabaseHas('schools_users', [
            'school_id' => $school->id,
            'user_id' => $result['item']['id']
        ]);


    }

    /** @test */
    public function it_updates_teacher()
    {
        $school = create(School::class);

        $user = create(User::class);

        $school->users()->attach([$user->id]);
        $requestData = [
            'login' => '',
            'first_name' => $this->faker->firstName,
            'middle_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];

        $this
            ->signInAsSchoolManager($school)
            ->putJson('school_management/api/teachers/' . $user->id, $requestData)
            ->assertStatus(200)
            ->assertJsonStructure(['item']);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'last_name' => $requestData['last_name']
        ]);
    }

    /** @test */
    public function it_destroys_teacher()
    {
        $school = create(School::class);

        $user = create(User::class, ['role' => Role::TEACHER]);
        $school->users()->attach([$user->id]);
        $this
            ->signInAsSchoolManager($school)
            ->deleteJson('school_management/api/teachers/' . $user->id)
            ->assertStatus(200);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);
    }
}
