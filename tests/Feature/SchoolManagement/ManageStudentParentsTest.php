<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\School;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageStudentParentsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_student_parents()
    {
        $school = create(School::class);
        $this
            ->signInAsSchoolManager($school)
            ->get('school_management/student/' . create(User::class)->id .'/parents')
            ->assertStatus(200);
    }

    /** @test */
    public function it_saves_student_parent()
    {
        $school = create(School::class);
        $parent = create(User::class, [
            'login' => 'unique!'
        ]);
        $student = create(User::class);

        $firstRequestData = [
            'parentType' => 'EXISTING',
            'login' => $parent->login
        ];

        $secondRequestData = [
            'last_name' => 'AAA',
            'first_name' => 'AAA',
            'middle_name' => 'AAA',
            'parentType' => 'NEW'
        ];

        $this
            ->signInAsSchoolManager($school)
            ->postJson(
                '/school_management/api/student/' . $student->id .  '/parents',
                $firstRequestData
            )
            ->assertStatus(200);

        $this->assertDatabaseHas('parents_students', [
            'parent_id' => $parent->id,
            'student_id' => $student->id,
        ]);

        $this
            ->signInAsSchoolManager($school)
            ->postJson(
                '/school_management/api/student/' . $student->id .  '/parents',
                $secondRequestData
            )
            ->assertStatus(200);

        $createdParent = User::query()->where('last_name', $secondRequestData['last_name'])->first();
        $this->assertNotNull($createdParent);
        $this->assertDatabaseHas('parents_students', [
            'parent_id' => $createdParent->id,
            'student_id' => $student->id,
        ]);
    }

    public function it_updates_roles()
    {
        
    }
}
