<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\Group;
use App\Models\School;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageGroupsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_shows_groups_list()
    {
        $school = create(School::class);

        create(Group::class, ['school_id' => $school->id], 10);
        $this
            ->signInAsSchoolManager($school)
            ->get('/school_management/groups')
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function it_stores_group()
    {
        $school = create(School::class);

        $year = 1;
        $title = '1-A';

        $this
            ->signInAsSchoolManager($school)
            ->post('/school_management/api/groups', [
                'year'  => $year,
                'title' => $title
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('groups', [
            'year'      => $year,
            'title'     => $title,
            'school_id' => $school->id
        ]);
    }

    /**
     * @test
     */
    public function it_updates_group()
    {
        $school = create(School::class);

        $year = 3;
        $title = '4-A';

        $group = create(Group::class, ['school_id' => $school->id]);

        $this
            ->signInAsSchoolManager($school)
            ->put('/school_management/api/groups/' . $group->id, [
                'year' => $year,
                'title' => $title
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('groups', [
            'year'      => $year,
            'title'     => $title,
            'school_id' => $school->id
        ]);
    }

    /**
     * @test
     */
    public function it_destroys_group()
    {
        $school = create(School::class);

        $group = create(Group::class, ['school_id' => $school->id]);

        $this
            ->signInAsSchoolManager($school)
            ->delete('/school_management/api/groups/' . $group->id)
            ->assertStatus(200);

        $this->assertDatabaseMissing('groups', [
            'id' => $group->id
        ]);
    }
}
