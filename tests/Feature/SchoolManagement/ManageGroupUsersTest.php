<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\Group;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageStudentsInGroupTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_lists_users()
    {
        $group = create(Group::class);

        $users = create(User::class, [], 10);

        $group->school->users()->sync($users->pluck('id'));
        $group->users()->sync($users->pluck('id'));

        $this
            ->signInAsSchoolManager($group->school)
            ->get('school_management/students/' . $group->id)
            ->assertStatus(200);
    }

    /** @test */
    public function it_stores_user()
    {
        $group = create(Group::class);

        $requestData = [
            'first_name' => 'Іван',
            'middle_name' => 'Васильович',
            'last_name' => 'Кущ',
        ];

        $this
            ->signInAsSchoolManager($group->school)
            ->postJson('school_management/api/students/' . $group->id, $requestData)
            ->assertStatus(200);

        $userInDatabase = User::where($requestData)->first();
        $this->assertNotNull($userInDatabase);
        $this->assertNotNull($userInDatabase->login);
        $this->assertEquals($userInDatabase->role, Role::STUDENT);

        $this->assertDatabaseHas('groups_users', [
            'group_id' => $group->id,
            'user_id' => $userInDatabase->id
        ]);
    }

    /** @test */
    public function it_updates_user()
    {
        $group = create(Group::class);
        $user = create(User::class);
        $group->users()->sync([$user->id]);
        $group->school->users()->sync([$user->id]);

        $newUserData = [
            'last_name' => 'New',
            'first_name' => 'New',
            'middle_name' => 'New',
            'login' => 'new' . time()
        ];

        $this
            ->signInAsSchoolManager($group->school)
            ->putJson('school_management/api/students/' . $group->id . "/" . $user->id, $newUserData)
            ->assertStatus(200)
            ->assertJsonStructure(['item' => ['id']]);

        $this->assertDatabaseHas('users', array_merge($newUserData, [
            'id' => $user->id
        ]));
    }

    /** @test */
    public function it_destroys_user()
    {
        $group = create(Group::class);
        $user = create(User::class);

        $group->users()->sync([$user->id]);
        $group->school->users()->sync([$user->id]);

        $this
            ->signInAsSchoolManager($group->school)
            ->deleteJson('school_management/api/students/' . $group->id . "/" . $user->id);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);

        $this->assertDatabaseMissing('groups_users', [
            'user_id' => $user->id
        ]);
    }
}
