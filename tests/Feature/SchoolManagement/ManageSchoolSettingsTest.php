<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\School;
use App\Models\SchoolSettings;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSchoolSettingsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_school_settings_page()
    {
        $school = create(School::class);
        $this
            ->signInAsSchoolManager($school)
            ->get('/school_management/settings')
            ->assertStatus(200);
    }

    /** @test */
    public function it_should_update_school_settings_for_current_year()
    {
        $school = create(School::class);

        create(SchoolSettings::class, [
            'school_id' => $school->id,
            'days_in_week' => 5
        ]);

        $requestData = [
            'days_in_week' => 6,
            'breaks'       => [
                [
                    'from'   => '2018-12-12',
                    'to'     => '2018-12-12',
                    'reason' => 'test',
                ]
            ]
        ];

        $this
            ->signInAsSchoolManager($school)
            ->putJson('/school_management/api/school_settings', $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('school_year_settings', [
            'school_id'    => $school->id,
            'days_in_week' => 6,
        ]);
    }

    /** @test */
    public function it_should_create_school_settings_when_it_is_not_created_yet()
    {
        $school = create(School::class);
        $requestData = [
            'days_in_week' => 6,
            'breaks'       => [
                [
                    'from'   => '2018-12-12',
                    'to'     => '2018-12-12',
                    'reason' => 'test',
                ]
            ]
        ];

        $this
            ->signInAsSchoolManager($school)
            ->putJson('/school_management/api/school_settings', $requestData)
            ->assertStatus(200);

        $this->assertDatabaseHas('school_year_settings', [
            'school_id'    => $school->id,
            'days_in_week' => 6,
        ]);
    }
}
