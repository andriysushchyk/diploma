<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OpenDashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_dashboard()
    {
        $school = create(School::class);
        $this
            ->signInAsSchoolManager($school)
            ->get('/school_management')
            ->assertStatus(200);
    }
}
