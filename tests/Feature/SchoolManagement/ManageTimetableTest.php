<?php

namespace Tests\Feature\SchoolManagement;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\TimetableItem;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageTimetableTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_opens_timetable_for_group()
    {
        $group = create(Group::class);
        $this
            ->signInAsSchoolManager($group->school)
            ->get('/school_management/timetable/' . $group->id)
            ->assertStatus(200);
    }

    /** @test */
    public function it_updates_timetable()
    {
        $group = create(Group::class);
        $firstSubject  = create(GroupSubject::class);
        $secondSubject = create(GroupSubject::class);

        $timetableSample = [
            [
                'group_subject_id' => $firstSubject->id,
                'day_number' => 1,
                'subject_number' => 1
            ],

            [
                'group_subject_id' => $secondSubject->id,
                'day_number' => 1,
                'subject_number' => 2
            ]
        ];

        $this
            ->signInAsSchoolManager($group->school)
            ->putJson('/school_management/api/timetable/' . $group->id, ['timetable' => $timetableSample])
            ->assertStatus(200);

        $this->assertDatabaseHas('timetable_items', $timetableSample[0]);
        $this->assertDatabaseHas('timetable_items', $timetableSample[1]);
    }
}
