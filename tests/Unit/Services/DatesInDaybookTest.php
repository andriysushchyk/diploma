<?php

namespace Tests\Unit\Services;

use App\Models\School;
use App\Models\SchoolSettings;
use App\Models\Year;
use App\Services\DatesInDaybook;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatesInDaybookTest extends TestCase
{
    use RefreshDatabase;

    public function datesProvider()
    {
        return [
            [
                Carbon::parse('2018-05-25'),
                Carbon::parse('2018-05-14'),
                Carbon::parse('2018-05-21'),
                null,
            ],
            [
                Carbon::parse("2018-05-06"),
                Carbon::parse("2018-04-23"),
                Carbon::parse("2018-05-07"),
                Carbon::parse("2018-05-14"),
                [Carbon::parse("2018-04-29"), Carbon::parse("2018-05-07")],
            ],
            [
                Carbon::parse("2018-05-06"),
                Carbon::parse("2018-04-23"),
                null,
                null,
                [Carbon::parse("2018-04-30"), Carbon::parse("2018-07-07")],
            ],
        ];
    }

    /**
     * @test
     * @dataProvider datesProvider
     */
    public function it_returns_correct_current_dates(
        $requestedDate,
        $expectedPrevWeekStart,
        $expectedCurrentWeekStart,
        $expectedNextWeekStart,
        ...$dates
    ) {
        $datesInDaybook = new DatesInDaybook();
        $school = create(School::class);
        $school->yearStart = Carbon::createFromDate(2017, 9, 1)->setTime(0, 0, 0, 0);
        $school->yearEnd = Carbon::createFromDate(2018, 5, 28)->setTime(0, 0, 0, 0);
        create(SchoolSettings::class, [
            'breaks'    => collect($dates)->map(function ($break) {
                return [
                    'from'   => $break[0]->toDateString(),
                    'to'     => $break[1]->toDateString(),
                    'reason' => 'test',
                ];
            }),
            'year_id'   => Year::current()->id,
            'school_id' => $school->id
        ]);


        $startOfCurrentWeek = $datesInDaybook->getStartOfCurrentStudyWeek($school, $requestedDate);
        $this->assertEquals(
            $expectedCurrentWeekStart ? $expectedCurrentWeekStart->toDateString() : null,
            $startOfCurrentWeek ? $startOfCurrentWeek->toDateString() : null
        );

        $startOfNextWeek = $datesInDaybook->getStartOfNextStudyWeek($school, $requestedDate);
        $this->assertEquals(
            $expectedNextWeekStart ? $expectedNextWeekStart->toDateString() : null,
            $startOfNextWeek ? $startOfNextWeek->toDateString() : null
        );

        $startOfPrevWeek = $datesInDaybook->getStartOfPreviousStudyWeek($school, $requestedDate);
        $this->assertEquals(
            $startOfPrevWeek ? $startOfPrevWeek->toDateString() : null,
            $expectedPrevWeekStart ? $expectedPrevWeekStart->toDateString() : null
        );
    }
}
