<?php

namespace Tests\Unit\Services;

use App\Models\Lesson;
use App\Models\LessonType;
use App\Models\TimetableItem;
use App\Services\JournalColumnsPaginator;
use App\Services\LessonTypeManager;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JournalColumnsPaginatorTest extends TestCase
{
    use RefreshDatabase;

    protected $lessons;

    protected function setUp()
    {
        parent::setUp();
        $lessonType = (new LessonTypeManager())->getTitle('test');
        $timetableItem = create(TimetableItem::class);

        Carbon::setTestNow(Carbon::parse("2018-01-01 00:00:00"));
        $this->lessons = collect([
            0  => new Lesson(Carbon::parse("2017-12-14 00:00:00"), $lessonType, $timetableItem, null),
            1  => new Lesson(Carbon::parse("2017-12-15 00:00:00"),  $lessonType, $timetableItem, null),
            2  => new Lesson(Carbon::parse("2017-12-16 00:00:00"), $lessonType, $timetableItem, null),
            3  => new Lesson(Carbon::parse("2017-12-17 00:00:00"), $lessonType, $timetableItem, null),
            4  => new Lesson(Carbon::parse("2017-12-18 00:00:00"), $lessonType, $timetableItem, null),
            5  => new Lesson(Carbon::parse("2017-12-19 00:00:00"), $lessonType, $timetableItem, null),
            6  => new Lesson(Carbon::parse("2017-12-20 00:00:00"), $lessonType, $timetableItem, null),
            7  => new Lesson(Carbon::parse("2017-12-21 00:00:00"), $lessonType, $timetableItem, null),
            8  => new Lesson(Carbon::parse("2017-12-22 00:00:00"), $lessonType, $timetableItem, null),
            9  => new Lesson(Carbon::parse("2017-12-23 00:00:00"), $lessonType, $timetableItem, null),
            10 => new Lesson(Carbon::parse("2017-12-24 00:00:00"), $lessonType, $timetableItem, null),
            11 => new Lesson(Carbon::parse("2017-12-25 00:00:00"), $lessonType, $timetableItem, null),
            12 => new Lesson(Carbon::parse("2018-01-02 00:00:00"), $lessonType, $timetableItem, null),
            13 => new Lesson(Carbon::parse("2018-01-03 00:00:00"), $lessonType, $timetableItem, null),
            14 => new Lesson(Carbon::parse("2018-01-04 00:00:00"), $lessonType, $timetableItem, null),
            15 => new Lesson(Carbon::parse("2018-01-05 00:00:00"), $lessonType, $timetableItem, null),
            16 => new Lesson(Carbon::parse("2018-01-06 00:00:00"), $lessonType, $timetableItem, null),
            17 => new Lesson(Carbon::parse("2018-01-06 00:00:00"), $lessonType, $timetableItem, null),
            18 => new Lesson(Carbon::parse("2018-01-06 00:00:00"), $lessonType, $timetableItem, null),
            19 => new Lesson(Carbon::parse("2018-01-06 00:00:00"), $lessonType, $timetableItem, null),
        ]);
    }

    /** @test */
    public function it_correctly_paginate_pages()
    {
        $journalColumnsPaginator = new JournalColumnsPaginator($this->lessons, 0);
        $this->assertEquals(
            $this->lessons->filter(function (Lesson $lesson, $index) {
                return $index >= 5 && $index <= 18;
            })->count(),
            $journalColumnsPaginator->currentPageItems()->count()
        );
        $this->assertTrue($journalColumnsPaginator->prevPageExists());
        $this->assertTrue($journalColumnsPaginator->nextPageExists());


        //TODO make tests last page.
    }
}
