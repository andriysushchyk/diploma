<?php

namespace Tests\Unit\Services;

use App\Models\ActivationRule;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use App\Services\AbilityToActivateSchoolChecker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AbilityToActivateSchoolCheckerTest extends TestCase
{
    use RefreshDatabase;

    /** @var AbilityToActivateSchoolChecker */
    protected $abilityToActivateSchoolChecker;

    public function setUp()
    {
        parent::setUp();
        $this->abilityToActivateSchoolChecker = new AbilityToActivateSchoolChecker();
    }

    /** @test */
    public function it_returns_correct_not_passed_rules()
    {
        $school = create(School::class);
        $notPassedRules = $this->abilityToActivateSchoolChecker->getNotPassedRules($school);
        $this->count(
            2,
            $this->abilityToActivateSchoolChecker->getNotPassedRules($school)
        );

        $this->assertTrue($notPassedRules->contains(function(ActivationRule $rule) {
            return $rule->getNotPassedText() == 'Додати вчителів';
        }));

        $this->assertTrue($notPassedRules->contains(function(ActivationRule $rule) {
            return $rule->getNotPassedText() == 'Додати навчальні групи';
        }));

        $users = create(User::class, [
            'role' => Role::TEACHER
        ], 2);
        $school->users()->sync($users->pluck('id')->toArray());

        $notPassedRules = $this->abilityToActivateSchoolChecker->getNotPassedRules($school);
        $this->count(
            1,
            $notPassedRules
        );
    }
}
