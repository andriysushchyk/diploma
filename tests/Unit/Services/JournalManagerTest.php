<?php

namespace Tests\Unit\Services;

use App\Models\AdditionalLesson;
use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Lesson;
use App\Models\LessonType;
use App\Models\Mark;
use App\Models\School;
use App\Models\TimetableItem;
use App\Models\User;
use App\Models\Year;
use App\Services\JournalManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JournalManagerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_returns_correct_lessons()
    {
        $school = create(School::class);
        $settings = $school->getSettingsForCurrentYear();
        $settings->breaks = [
            ['from' => '2019-05-20', 'to' => '2019-05-26', 'reason' => 'test'],
        ];
        $settings->school_id = $school->id;
        $settings->year_id = Year::current()->id;
        $settings->save();

        $group = create(Group::class, ['school_id' => $school->id]);

        $groupSubject = create(GroupSubject::class, [
            'group_id' => $group->id
        ]);

        create(TimetableItem::class, [
            'day_number' => 1,
            'group_subject_id' => $groupSubject->id
        ]);

        create(TimetableItem::class, [
            'day_number' => 2,
            'group_subject_id' => $groupSubject->id
        ]);

        $journalColumns = new JournalManager();
        $columns = $journalColumns->getJournalColumns($groupSubject);
        $this->assertFalse($columns->contains(function(Lesson $lesson) {
            return $lesson->date->format("Y-m-d") == "2019-05-20";
        }));

        $this->assertTrue($columns->contains(function(Lesson $lesson) {
            return $lesson->date->format("Y-m-d") == "2019-05-28";
        }));
    }

    /** @test */
    public function it_returns_correct_lessons_with_additional_lessons()
    {
        $school = create(School::class);
        $settings = $school->getSettingsForCurrentYear();
        $settings->breaks = [
            ['from' => '2019-05-20', 'to' => '2019-05-26', 'reason' => 'test'],
        ];
        $settings->school_id = $school->id;
        $settings->year_id = Year::current()->id;
        $settings->save();

        $group = create(Group::class, ['school_id' => $school->id]);

        $groupSubject = create(GroupSubject::class, [
            'group_id' => $group->id
        ]);

        create(TimetableItem::class, [
            'day_number' => 1,
            'group_subject_id' => $groupSubject->id
        ]);

        create(TimetableItem::class, [
            'day_number' => 2,
            'group_subject_id' => $groupSubject->id
        ]);

        create(AdditionalLesson::class, [
            'group_subject_id' => $groupSubject->id,
            'date' => '2018-05-06'
        ]);

        $journalColumns = new JournalManager();
        $columns = $journalColumns->getJournalColumns($groupSubject);

        $additionalColumns = ($columns->filter(function(Lesson $lesson) {
            return $lesson->additionalLessonID;
        }));

        $this->assertCount(1, $additionalColumns);

        $this->assertFalse($columns->contains(function(Lesson $lesson) {
            return $lesson->date->format("Y-m-d") == "2019-05-20";
        }));

        $this->assertTrue($columns->contains(function(Lesson $lesson) {
            return $lesson->date->format("Y-m-d") == "2019-05-28";
        }));
    }

    /** @test */
    public function it_returns_marks()
    {
        $journalManager = new JournalManager();
        $groupSubject = create(GroupSubject::class);
        $user = create(User::class);
        DB::table('groups_users')->insert([
            'group_id' => $groupSubject->id,
            'user_id' => $user->id
        ]);
        create(Mark::class, [
            'user_id' => $user->id
        ]);
        $this->assertCount(0, $journalManager->getMarks($groupSubject));
    }
}
