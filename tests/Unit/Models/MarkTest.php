<?php

namespace Tests\Unit\Models;

use App\Models\AdditionalLesson;
use App\Models\Mark;
use App\Models\TimetableItem;
use SebastianBergmann\Timer\TimerTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarkTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_lesson_type()
    {
        $mark = create(Mark::class, [
            'mark_lesson_id' => create(AdditionalLesson::class)->id,
            'mark_lesson_type' => (new AdditionalLesson())->getMorphClass()
        ]);
        $this->assertInstanceOf(AdditionalLesson::class, $mark->markLesson);

        $mark = create(Mark::class, [
            'mark_lesson_id' => create(TimetableItem::class)->id,
            'mark_lesson_type' => (new TimetableItem())->getMorphClass()
        ]);
        $this->assertInstanceOf(TimetableItem::class, $mark->markLesson);
    }
}
