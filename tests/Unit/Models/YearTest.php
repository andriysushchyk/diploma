<?php

namespace Tests\Unit\Models;

use App\Models\Year;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class YearTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_returns_correct_current_year()
    {
        Carbon::setTestNow(Carbon::now()->year(2012)->month(9)->day(12));
        $this->assertEquals(Year::current()->title, "2012/2013");
        $this->assertNotFalse(Year::current()->is_active);

        Year::query()->delete();

        Carbon::setTestNow(Carbon::now()->year(2018)->month(6)->day(12));
        $this->assertEquals(Year::current()->title, "2017/2018");
        $this->assertNotFalse(Year::current()->is_active);

        Carbon::setTestNow();
    }
}
