<?php

namespace Tests\Unit\Models;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\School;
use App\Models\TimetableItem;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_many_timetable_items_through_subject_groups()
    {
        $group = create(Group::class);
        $groupSubjects = create(GroupSubject::class, [
            'group_id' => $group->id
        ], 2);
        $firstItem = create(TimetableItem::class, [
            'group_subject_id' => $groupSubjects[0]->id
        ]);

        $secondItem = create(TimetableItem::class, [
            'group_subject_id' => $groupSubjects[1]->id
        ]);


        $this->assertInstanceOf(Collection::class, $group->timetableItems);
        $this->assertEquals([$firstItem->id, $secondItem->id], $group->timetableItems->pluck('id')->toArray());
    }
}
