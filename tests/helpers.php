<?php

function create($modelClassName, $data = [], $count = null)
{
    return factory($modelClassName, $count)->create($data);
}

function make($modelClassName, $data = [], $count = null)
{
    return factory($modelClassName, $count)->make($data);
}
