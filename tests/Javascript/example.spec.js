import {mount} from 'vue-test-utils'
import expect from 'expect'
import VStaticInput from '../../resources/assets/js/components/VStaticInput'

describe('VStaticInput', () => {
    let component;

    beforeEach(() => {
        component = mount(VStaticInput)
    });

    it('contains example', () => {
        expect(component.html()).toContain('div')
    })
})
