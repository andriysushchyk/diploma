<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Guest'], function () {
    Route::get('', 'GuestController@home');
    Route::get('join', 'SchoolApplicationController@create');
    Route::get('applications/{school_application}', 'SchoolApplicationController@show');
    Route::post('/api/school_applications', 'SchoolApplicationController@store');
});

Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout');
});


Route::namespace('SchoolManagement')->prefix('school_management')->middleware([
    'auth',
    'school_manager'
])->group(function () {

    Route::namespace('Web')->group(function () {
        Route::get('', 'DashboardController@index')->name('admin.dashboard.index');
        Route::get('settings', 'SettingsController@index')->name('admin.settings.index');
        Route::get('groups', 'GroupController@index')->name('admin.groups.index');
        Route::get('timetable/{group}', 'GroupTimetableController@edit')->name('timetable.index');
        Route::get('teachers', 'TeacherController@index')->name('teachers.index');
        Route::get('students/{group}', 'StudentController@index')->name('students.index');
        Route::get('student/{user}/parents', 'ParentController@index')->name('parents.index');
        Route::post('activate', 'DashboardController@activate');
        Route::get('download', 'DashboardController@usersList');
    });


    Route::prefix('api')->namespace('Api')->group(function () {
        Route::resource('groups', 'GroupController', ['only' => ['store', 'update', 'destroy']]);
        Route::post('students/{group}', 'GroupUserController@store');
        Route::put('students/{group}/{user}', 'GroupUserController@update');
        Route::delete('students/{group}/{user}', 'GroupUserController@destroy');

        Route::put('school_settings', 'SchoolSettingsController@update');

        Route::resource('teachers', 'TeacherController', [
                'only' => ['store', 'update', 'destroy'],
                'parameters' => ['teachers' => 'user']
            ]
        );

        Route::post('student/{user}/parents', 'ParentController@store');

        Route::post('subjects/group/{group}', 'GroupSubjectController@store');
        Route::put('subjects/{subject}', 'GroupSubjectController@update');
        Route::delete('subjects/{subject}', 'GroupSubjectController@destroy');
        Route::put('timetable/{group}', 'GroupTimetableController@update');
    });

});


Route::group(['middleware' => 'auth'], function () {
    Route::redirect('school', 'school/news');
    Route::get('school/news', 'School\NewsController@index')->name('school.news.index');


    Route::get('school/news/create', 'School\NewsController@create')->name('school.news.create');
    Route::get('school/news/{news_item}', 'School\NewsController@show')->name('school.news.show');
    Route::get('school/daybook/{user?}', 'School\DayBookController@index')->name('school.daybook.index');
    Route::get('school/messages', 'School\ConservationController@index')->name('school.messages.index');
    Route::get('school/messages/{user}', 'School\ConservationController@show');
    Route::get('school/messages/{user}/info', 'School\ConservationController@info');
    Route::get('school/users/{user_login}', 'School\UserController@show');
    Route::get('school/education_materials/{group_subject}', 'School\EducationalMaterialsController@index');
    Route::get('school/education_materials/show/{education_material}', 'School\EducationalMaterialsController@show');
    Route::get('school/ed_materials/{education_material}/download', 'School\EducationalMaterialsController@download');
    Route::post('school/education_materials/{group_subject}', 'School\EducationalMaterialsController@store');
    Route::delete('school/education_materials/{group_subject}', 'School\EducationalMaterialsController@destroy');

    Route::post('school/messages/{user}', 'School\ConservationController@store')
         ->name('school.messages.show');

    Route::get('school/journal/{group_subject}', 'School\JournalController@show')->name('school.journal.show');

    Route::get('school/profile', 'School\ProfileController@show')->name('school.profile.show');

    Route::put('school/api/profile', 'School\Api\UserProfileController@update');
    Route::put('school/api/profile/password', 'School\Api\UserPasswordController@update');
    Route::post('school/api/news', 'School\NewsController@store')->name('school.news.store');
    Route::delete('school/api/news/{news_item}', 'School\NewsController@destroy')->name('school.news.delete');

    Route::put('school/api/homework', 'School\Api\HomeworkController@update');
    Route::post('school/api/lessons', 'School\Api\LessonController@store');
    Route::delete('school/api/lessons', 'School\Api\LessonController@destroy');
    Route::post('school/api/marks', 'School\Api\MarkController@store');
    Route::delete('school/api/marks/{mark}', 'School\Api\MarkController@destroy');
});

