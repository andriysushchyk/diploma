<?php

namespace App\ViewComposers;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class MenuViewComposer
{
    public function compose(View $view)
    {
        $newsMenuItem = ['icon' => 'fa fa-newspaper-o', 'title' => 'Новини', 'link' => route('school.news.index')];

        $messagesMenuItem = [
            'icon'  => 'fa fa-comment',
            'title' => 'Повідомлення',
            'link'  => route('school.messages.index')
        ];

        $dayBookMenuItem = ['icon' => 'fa fa-book', 'title' => 'Щоденник', 'link' => route('school.daybook.index')];

        if (Auth::user()->role === ROLE::PARENT && Auth::user()->childs->count() > 1) {
            $dayBookMenuItem = [
                'icon'     => 'fa fa-book',
                'title'    => 'Щоденник',
                'subitems' => Auth::user()->childs->map(function (User $user) {
                    return [
                        'title' => $user->first_name,
                        'link' => route('school.daybook.index', [$user->id])
                    ];
                })
            ];

        } elseif (Auth::user()->role === Role::PARENT) {
            $dayBookMenuItem = [
                'icon' => 'fa fa-book',
                'title' => 'Щоденник',
                'link' => route('school.daybook.index', Auth::user()->childs->first()->id)
            ];
        }

        if (Auth::user()->role === ROLE::TEACHER) {
            $teacher = Auth::user();
            $groups = $teacher->groupSubjects;
            if ($school = Auth::user()->schoolManaged()) {
                $groups = $school->groups->map(function (Group $group) {
                    return $group->subjects;
                })->flatten();
            }

            $teacherGroups = $groups
                ->groupBy('group_id')
                ->map(function ($subjects) {
                    return [
                        'title' => $subjects->first()->group->title,
                        'link' => '',
                        'subitems' => $subjects->map(function (GroupSubject $groupSubject) {
                            return [
                                'title' => $groupSubject->getTitle(),
                                'link' => route('school.journal.show', [$groupSubject->id]),
                            ];
                        })
                    ];
                });
        }

        $journalMenuItem = [
            'icon'     => 'fa fa-book',
            'title'    => 'Журнал',
            'link'     => '',
            'subitems' => (isset($teacherGroups) && count($teacherGroups))
                ? $teacherGroups
                : [['title' => 'Список пустий', 'link' => '']]

        ];

        $role = Auth::user()->role;

        if ($role == Role::TEACHER) {
            $menu = [
                'news'     => $newsMenuItem,
                'journal'  => $journalMenuItem,
                'messages' => $messagesMenuItem,
            ];

        } else {
            $menu = [
                'news'     => $newsMenuItem,
                'daybook'  => $dayBookMenuItem,
                'messages' => $messagesMenuItem,
            ];
        }
        $view->with('menu', $menu);
    }
}
