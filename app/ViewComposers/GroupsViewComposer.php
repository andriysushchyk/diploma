<?php


namespace App\ViewComposers;

use App\Models\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class GroupsViewComposer
{
    public function compose(View $view)
    {
        $groups = Group::query()
                       ->where('school_id', Auth::user()->schoolManaged()->id)
                       ->get()
                       ->groupBy('year');

        $view->with('groupsInNavMenu', $groups);
    }
}
