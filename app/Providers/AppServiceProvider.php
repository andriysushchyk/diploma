<?php

namespace App\Providers;

use App\ViewComposers\GroupsViewComposer;
use App\ViewComposers\MenuViewComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('uk_UA');
        });

        View::composer('school.base', MenuViewComposer::class);

        View::composer('school_management.base', GroupsViewComposer::class);
    }
}
