<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('successJson', function ($data = []) {
            return response()->json(array_merge([
                'message' => 'All OK'
            ], $data));
        });

        Response::macro('failedJson', function ($error, $data = [], $status = 400) {
            return response()->json(array_merge([
                'error' => $error
            ], $data), $status);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
