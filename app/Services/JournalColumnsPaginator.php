<?php

namespace App\Services;

use App\Models\Lesson;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class JournalColumnsPaginator
{
    protected $allYearColumns;

    protected $currentPage;

    protected $mediumIndex;

    public function __construct(Collection $columns, int $page = 0)
    {
        $this->allYearColumns = $columns;
        $this->currentPage = $page;
        $now = Carbon::now();
        $lessonMostApproxToCurrent = $this
            ->allYearColumns
            ->reduce(function (?Lesson $mostApproxLesson, Lesson $lesson) use ($now) {
                if ($mostApproxLesson === null) {
                    return $lesson;
                }
                if ($lesson->date->diffInDays($now) < $mostApproxLesson->date->diffInDays($now)) {
                    return $lesson;
                }

                return $mostApproxLesson;
            }, null);

        $this->mediumIndex = 0;
        $this->allYearColumns->each(function (Lesson $lesson, $i) use ($lessonMostApproxToCurrent) {
            if ($lesson === $lessonMostApproxToCurrent) {
                $this->mediumIndex = $i;
                return false;
            }
        });
    }

    private function getForPage($page)
    {
        $index = $this->mediumIndex + ($page * 14);
        return $this->allYearColumns->filter(function (Lesson $lesson, $lessonIndex) use ($index) {
            return ($lessonIndex + 7) >= $index
                && ($lessonIndex - 6) <= $index;
        })->values();
    }

    public function currentPageItems()
    {
        return $this->getForPage($this->currentPage);
    }

    public function nextPageExists()
    {
        return $this->getForPage($this->currentPage + 1)->isNotEmpty();
    }

    public function prevPageExists()
    {
        return $this->getForPage($this->currentPage - 1)->isNotEmpty();
    }

    public function dateStart()
    {
        $columns = $this->currentPageItems();
        return $columns->first()
            ? $columns->first()->date
            : null;
    }

    public function dateEnd()
    {
        $columns = $this->currentPageItems();
        return $columns->last()
            ? $columns->last()->date
            : null
        ;
    }

    public function page()
    {
        return $this->currentPage;
    }
}
