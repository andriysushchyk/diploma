<?php


namespace App\Services;

use App\Models\Group;
use App\Models\Mark;
use App\Models\User;
use Carbon\Carbon;

class DailyMarksService
{
    public function getMarks(Carbon $date, User $user)
    {
        $marks = Mark::query()
                     ->where('date', $date->toDateString())
                     ->where('user_id', $user->id)
                     ->get();
        $group = Group::whereHas('users', function ($q) use ($user) {
            $q->where('users.id', $user->id);
        })->first();

        $result = [];
        $timetableItems = $group->timetableItems->where('day_number', $date->dayOfWeek);

        foreach ($marks as $mark) {
            $timetableItem = $timetableItems->where('group_subject_id', $mark->markLesson->group_subject_id)->first();
            if (!$timetableItem) {
                continue;
            }
            $result[] = [
                'value'          => $mark->markValue->title,
                'type'           => is_string($mark->mark_type->getTitle())
                    ? $mark->mark_type->getTitle()
                    : $mark->mark_type->getTitle()->getTitle(),
                'date_formatted' => $date->format('d/m'),
                'subject_number' => $timetableItem->subject_number
            ];
        }
        return collect($result);
    }
}
