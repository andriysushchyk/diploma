<?php

namespace App\Services;

use App\Models\AdditionalLesson;
use App\Models\GroupSubject;
use App\Models\Lesson;
use App\Models\LessonType;
use App\Models\Mark;
use App\Models\School;
use App\Models\SchoolSettings;
use App\Models\TimetableItem;
use App\Models\YearBreak;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class JournalManager
{
    const COLUMNS_PER_PAGE = 14;

    /**
     * @param GroupSubject $subject
     * @return \Illuminate\Support\Collection
     */
    public function getJournalColumns(GroupSubject $subject)
    {
        $school = $subject->group->school;
        $yearStart = $school->yearStart;
        $yearEnd = $school->yearEnd;

        $date = $yearStart->copy();
        $subjectInTimetableItems = $subject->timetableItems;

        $result = collect([]);

        while ($date->lessThan($yearEnd)) {
            $result = $result->concat($subjectInTimetableItems->map(function (TimetableItem $timetableItem) use ($date
            ) {
                return [
                    $date->copy()->startOfWeek()->addDays($timetableItem->day_number - 1),
                    $timetableItem
                ];
            })->filter(function (array $data) use ($yearStart, $yearEnd, $school) {
                return $this->shouldDateBeAddedToJournal($data[0], $school);
            })->map(function ($data) {
                return new Lesson($data[0], LessonTypeManager::getDefault(), $data[1]->id, null);
            }));

            $date->addWeek();
        }

        $result = $result->merge($subject->additionalLessons->map(function (AdditionalLesson $additionalLesson) {
            return new Lesson(
                Carbon::parse($additionalLesson->date),
                (new LessonTypeManager())->getTitle($additionalLesson->lesson_type),
                null,
                $additionalLesson->id
            );
        }));

        return $result->sortBy(function (Lesson $lesson) {
            return $lesson->date->timestamp;
        })->values();
    }

    /**
     * @param Carbon $date
     * @param School $school
     * @return bool
     */
    public function shouldDateBeAddedToJournal(Carbon $date, School $school)
    {
        return $date->greaterThanOrEqualTo($school->yearStart)
             && $date->lessThan($school->yearEnd)
             && !$school->getSettingsForCurrentYear()->getBreaks()->contains(function (YearBreak $break) use ($date) {
                 return $break->from->lessThanOrEqualTo($date)
                     && $break->to->greaterThanOrEqualTo($date)
                     ;
             });
    }

    public function getMarks(GroupSubject $subject)
    {
        return Mark::query()
                   ->whereIn('user_id', $subject->group->users->pluck('id'))
                   ->get()
                   ->filter(function (Mark $mark) use ($subject) {
                    if ($mark->markLesson && $mark->markLesson->group_subject_id) {
                        return $mark->markLesson->group_subject_id == $subject->id;
                    }

                       return false;
                   })->values();
    }
}
