<?php

namespace App\Services;

class UkranianCalendarResolver
{
    protected $months = [
        1  => 'Січня',
        2  => 'Лютого',
        3  => 'Березня',
        4  => 'Квітня',
        5  => 'Травня',
        6  => 'Червня',
        7  => 'Липня',
        8  => 'Серпня',
        9  => 'Вересня',
        10 => 'Жовтня',
        11 => 'Листопада',
        12 => 'Грудня',
    ];

    protected $weeks = [
        1 => 'Понеділок',
        2 => 'Вівторок',
        3 => 'Середа',
        4 => 'Четвер',
        5 => 'П\'ятниця',
        6 => 'Субота',
        7 => 'Неділя'
    ];

    public function getMonthName($monthNumber)
    {
        return $this->months[$monthNumber];
    }

    public function getWeekdayTitle($dayNumber)
    {
        return $this->weeks[$dayNumber];
    }
}
