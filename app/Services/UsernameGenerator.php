<?php

namespace App\Services;

use App\Models\User;

class UsernameGenerator
{
    /**
     * @param User $user
     * @return string
     */
    public function generate(User $user)
    {
        $str = "{$user->last_name}_{$user->first_name}_{$user->middle_name}";
        $result = '';
        $trans = $this->getTransriptions();
        for ($i = 0; $i < mb_strlen($str); $i++) {
            $letter = mb_strtolower(mb_substr($str, $i, 1));
            if ($letter == '_') {
                $result .= '_';

            } elseif (array_has($trans, mb_strtolower($letter))) {
                $result .= $trans[mb_strtolower($letter)];
            }
        }

        if (User::query()->where('login', $result)->count()) {
            $result .= '_' . User::count();
        }

        return $result;
    }

    protected function getTransriptions()
    {
        return [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'h',
            'ґ' => 'g',
            'д' => 'd',
            'е' => 'e',
            'є' => 'ye',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'y',
            'і' => 'i',
            'ї' => 'i',
            'й' => 'i',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'kh',
            'ц' => 'ts',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ю' => 'yu',
            'я' => 'ya',
        ];
    }
}
