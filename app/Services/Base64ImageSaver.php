<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class Base64ImageSaver
{
    public function save($base64ImageData)
    {
        $fileName = 'images/' . time() . uniqid() . '.jpg';

        @list($type, $base64ImageData) = explode(';', $base64ImageData);
        @list(, $base64ImageData) = explode(',', $base64ImageData);

        if ($base64ImageData != "") { // storing image in storage/app/public Folder
            Storage::disk('public')->put($fileName, base64_decode($base64ImageData));
        }

        return $fileName;
    }
}
