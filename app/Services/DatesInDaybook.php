<?php

namespace App\Services;

use App\Models\School;
use App\Models\YearBreak;
use Carbon\Carbon;

class DatesInDaybook
{
    public function getBookedDays(School $school)
    {
        $bookedDays = [];

        $school->getSettingsForCurrentYear()->getBreaks()->each(function ($break) use (&$bookedDays) {
            $breakFromDate = $break->from->copy();
            while ($breakFromDate->lte($break->to)) {
                $bookedDays[$breakFromDate->format("Y-m-d")] = true;
                $breakFromDate->addDay();
            }
        });

        return $bookedDays;
    }

    /**
     * @param School $school
     * @param Carbon $date
     * @return Carbon|static
     */
    public function getStartOfCurrentStudyWeek(School $school, Carbon $date)
    {

        $date = $date->copy()->setTime(0, 0, 0, 0);
        $bookedDays = $this->getBookedDays($school);

        $isCurrentWeekFree = false;
        $date->startOfWeek();

        if ($date->lessThan($school->yearStart)) {
            $date = $school->yearStart->copy()->startOfWeek();
        }

        while (!$isCurrentWeekFree && $date->lessThan($school->yearEnd)) {


            for ($i = 0; $i < $school->getSettingsForCurrentYear()->days_in_week; $i++) {
                $checkedDate = $date->copy()->addDay($i);
                if (!isset($bookedDays[$checkedDate->toDateString()])) {
                    $isCurrentWeekFree = true;
                    break;
                }
            }

            if (!$isCurrentWeekFree) {
                $date->addWeek();
            }
        }

        return $isCurrentWeekFree
            ? $date
            : null;
    }

    /**
     * @param School $school
     * @param Carbon $date
     * @return null|Carbon
     */
    public function getStartOfPreviousStudyWeek(School $school, Carbon $date)
    {
        $date = $date->copy()->setTime(0, 0, 0, 0);
        $bookedDays = $this->getBookedDays($school);

        $isCurrentWeekFree = false;
        $date->startOfWeek()->subWeek();

        while (!$isCurrentWeekFree && $date->greaterThanOrEqualTo($school->yearStart)) {
            for ($i = 0; $i < $school->getSettingsForCurrentYear()->days_in_week; $i++) {
                $checkedDate = $date->copy()->addDay($i);
                if (!isset($bookedDays[$checkedDate->toDateString()])) {
                    $isCurrentWeekFree = true;
                    break;
                }
            }

            if (!$isCurrentWeekFree) {
                $date->subWeek();
            }
        }

        return $isCurrentWeekFree
            ? $date
            : null;
    }

    public function getStartOfNextStudyWeek(School $school, Carbon $date)
    {
        $date = $date->copy()->setTime(0, 0, 0, 0);
        $currentWeekStart = $this->getStartOfCurrentStudyWeek($school, $date);
        if (!is_null($currentWeekStart)) {
            return $this->getStartOfCurrentStudyWeek($school, $currentWeekStart->addWeek());
        }
        return null;
    }
}
