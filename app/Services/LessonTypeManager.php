<?php


namespace App\Services;

use App\Models\LessonType;

class LessonTypeManager
{
    const APPELATION = 'appelation';

    const LISTENING = 'listening';

    const DPA = 'dpa';

    const DICTANT = 'dictant';

    const DIALOG = 'dialog';

    const HOMEWORK = 'homework';

    const ZALIK = 'zalik';

    const ZHOSHIT = 'shoshit';

    const KR = 'kr';

    const LR = 'lr';

    const NAPAMYAT = 'napamyat';

    const PEREKAZ = 'perekaz';

    const PISMO = 'pismo';

    const POTOCHNA = 'potochna';

    const PR = 'pr';

    const RICHNA = 'richna';

    const SAMOSTIYNA = 'samostiyna';

    const SEMESTROVA = 'semestrova';

    const SEMINAR = 'seminar';

    const SKORIHOVANA = 'skorihovana';

    const TVIR = 'tvir';

    const TEMA = 'tema';

    const TEST = 'test';

    const CHITANNYA = 'chitannya';

    public function getAll()
    {
        return collect([
            new LessonType(self::APPELATION, 'Апеляція'),
            new LessonType(self::LISTENING, 'Аудіювання'),
            new LessonType(self::DPA, 'ДПА'),
            new LessonType(self::DICTANT, 'Диктант'),
            new LessonType(self::DIALOG, 'Діалог'),
            new LessonType(self::HOMEWORK, 'Домашнє завдання'),
            new LessonType(self::ZALIK, 'Залік'),
            new LessonType(self::ZHOSHIT, 'Зошит'),
            new LessonType(self::KR, 'Контрольна робота'),
            new LessonType(self::LR, 'Лабораторна робота'),
            new LessonType(self::NAPAMYAT, 'Напам\'ять'),
            new LessonType(self::PEREKAZ, 'Переказ'),
            new LessonType(self::PISMO, 'Письмо'),
            new LessonType(self::POTOCHNA, 'Поточна'),
            new LessonType(self::PR, 'Практична робота'),
            new LessonType(self::RICHNA, 'Річна'),
            new LessonType(self::SAMOSTIYNA, 'Самостійна робота'),
            new LessonType(self::SEMESTROVA, 'Семестрова робота'),
            new LessonType(self::SEMINAR, 'Семінар'),
            new LessonType(self::SKORIHOVANA, 'Скоригована'),
            new LessonType(self::TVIR, 'Твір'),
            new LessonType(self::TEMA, 'Тематична'),
            new LessonType(self::TEST, 'Тест'),
            new LessonType(self::CHITANNYA, 'Читання'),
        ]);
    }

    public static function getDefault()
    {
        return new LessonType(self::POTOCHNA, 'Поточна');
    }

    public function getTitle($value)
    {
        $findedType = $this->getAll()->first(function (LessonType $lessonType) use ($value) {
            return $value == $lessonType->getValue();
        });

        return $findedType ?? new LessonType($value, "Інше");
    }
}
