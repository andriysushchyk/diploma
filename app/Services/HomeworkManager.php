<?php


namespace App\Services;

use App\Models\Homework;
use App\Models\TimetableItem;
use Carbon\Carbon;

class HomeworkManager
{
    public function getHomeworkContent($timetableItemId, Carbon $date)
    {
        $homework = Homework::query()
            ->where('group_subject_id', TimetableItem::find($timetableItemId)->group_subject_id)
            ->where('date', $date->toDateString())
            ->first();

        return $homework ? $homework->work : '';
    }
}
