<?php

namespace App\Services;

use App\Models\ActivationRule;
use App\Models\Role;
use App\Models\School;

class AbilityToActivateSchoolChecker
{
    /**
     * @param School $school
     * @return \Illuminate\Support\Collection
     */
    public function getNotPassedRules(School $school)
    {
        $result = collect([]);

        $result->push(new ActivationRule(
            "Додати вчителів",
            route('teachers.index'),
            $school->users()->where('role', Role::TEACHER)->count() > 1
        ));

        $result->push($isAddedGroupsRule = new ActivationRule(
            "Додати навчальні групи",
            route('admin.groups.index'),
            $school->groups()->count()
        ));

        

        return collect($result->filter(function (ActivationRule $rule) {
            return !$rule->getIsPassed();
        }));
    }
}
