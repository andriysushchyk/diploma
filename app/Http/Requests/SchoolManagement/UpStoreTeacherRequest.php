<?php

namespace App\Http\Requests\SchoolManagement;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class UpStoreTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|required',
            'middle_name' => 'string|required',
            'last_name' => 'string|required',
        ];
    }

    public function getData()
    {
        return array_merge($this->only(['first_name', 'middle_name', 'last_name']), [
            'role' => Role::TEACHER,
        ]);
    }
}
