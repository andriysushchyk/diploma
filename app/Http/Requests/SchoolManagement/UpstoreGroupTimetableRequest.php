<?php

namespace App\Http\Requests\SchoolManagement;

use App\Models\TimetableItem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class UpstoreGroupTimetableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timetable'                    => 'array|present',
            'timetable.*.group_subject_id' => 'required',
            'timetable.*.day_number'       => 'required',
            'timetable.*.subject_number'   => 'required',
        ];
    }

    /**
     * @return Collection
     */
    public function getTimetableItems()
    {
        return collect($this->input('timetable'))->map(function ($timetableItemData) {
            return new TimetableItem(array_only($timetableItemData, [
                'group_subject_id',
                'day_number',
                'subject_number'
            ]));
        });
    }
}
