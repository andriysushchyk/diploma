<?php

namespace App\Http\Requests\SchoolManagement;

use Illuminate\Foundation\Http\FormRequest;

class UpstoreSubjectInGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'teacher_id' => 'integer|nullable'
        ];
    }

    public function getData()
    {
        return $this->only(['title', 'teacher_id']);
    }
}
