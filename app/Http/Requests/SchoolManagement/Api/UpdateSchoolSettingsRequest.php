<?php

namespace App\Http\Requests\SchoolManagement\Api;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSchoolSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'days_in_week' => 'required|in:5,6',
            'breaks' => 'array',
            'breaks.*.from' => 'required|date_format:Y-m-d',
            'breaks.*.to' => 'required|date_format:Y-m-d',
            'breaks.*.reason' => 'required|string',
        ];
    }

    public function getData()
    {
        return $this->only(['days_in_week', 'breaks']);
    }
}
