<?php

namespace App\Http\Requests\School;

use App\Models\AdditionalLesson;
use App\Models\TimetableItem;
use Illuminate\Foundation\Http\FormRequest;

class StoreMarkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'user_id' => 'required|exists:users,id',
            'mark_value_id' => 'required|exists:mark_values,id'
        ];
    }

    public function getData()
    {
        if ($this->input('additional_lesson_id')) {
            $markLessonType = (new AdditionalLesson())->getMorphClass();
            $markLessonID = $this->input('additional_lesson_id');

        } else {
            $markLessonType = (new TimetableItem())->getMorphClass();
            $markLessonID = $this->input('timetable_item_id');
        }

        return [
            'mark_lesson_id' => $markLessonID,
            'mark_lesson_type' => $markLessonType,
            'mark_value_id' => $this->input('mark_value_id'),
            'user_id' => $this->input('user_id'),
            'date' => $this->input('date')
        ];
    }
}
