<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdditionalLessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_subject_id' => 'required|exists:groups_subjects,id',
            'lesson_type' => 'required|string',
            'date' => 'required|date_format:Y-m-d'
        ];
    }

    public function getData()
    {
        return $this->only(['group_subject_id', 'lesson_type', 'date']);
    }
}
