<?php

namespace App\Http\Requests\School;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required|string'
        ];
    }

    public function getData()
    {
        return [
            'body'    => $this->input('body'),
            'to_id'   => $this->route('user'),
            'to_type' => (new User())->getMorphClass(),
            'from_user_id' => $this->user()->id,
        ];
    }

    public function messages()
    {
        return [
            'body.*' => 'Повідомлення пусте'
        ];
    }
}
