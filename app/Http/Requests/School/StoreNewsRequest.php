<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required',
            'preview_text' => 'required',
            'full_text'    => 'required',
            'image'        => 'required'
        ];
    }

    public function getData()
    {
        return $this->only(['title', 'full_text' , 'image', 'preview_text']);
    }

    public function getImage()
    {
        return $this->input('image');
    }
}
