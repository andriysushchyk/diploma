<?php

namespace App\Http\Requests\Lessons;

use Illuminate\Foundation\Http\FormRequest;

class DeleteLessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ''
        ];
    }

    public function getAdditionalLessonID()
    {
        return $this->input('additional_lesson_id');
    }

    public function getTimetableItemID()
    {
        return $this->input('timetable_item_id');
    }

    public function getDate()
    {
        return $this->input('date');
    }
}
