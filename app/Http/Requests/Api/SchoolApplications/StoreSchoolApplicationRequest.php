<?php

namespace App\Http\Requests\Api\SchoolApplications;

use Illuminate\Foundation\Http\FormRequest;

class StoreSchoolApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_city_id' => 'required|exists:cities,id',
            'school_organization_type_id' => 'required|exists:school_organization_types,id',
            'school_short_name' => 'required|string',
            'school_full_name' => 'required|string',
            'school_phone_number' => 'required|string',


            'sender_first_name' => 'required|string',
            'sender_middle_name' => 'required|string',
            'sender_last_name' => 'required|string',
            'sender_position' => 'required|string',
            'sender_phone_number' => 'required|string',
            'sender_email' => 'required|string',
        ];
    }

    public function getApplicationData()
    {
        return $this->only(array_keys($this->rules()));
    }

    public function attributes()
    {
        return [
            'school_short_name' => trans('school.short_name'),
            'school_full_name' =>  trans('school.full_name'),
            'school_phone_number' =>  trans('common.phone_number'),

            'school_city_id' => trans('common.city'),
            'school_organization_type_id' => trans('school.organization_type'),

            'sender_first_name' => trans('common.first_name'),
            'sender_middle_name' => trans('common.middle_name'),
            'sender_last_name' => trans('common.last_name'),
            'sender_position' => trans('common.position'),
            'sender_phone_number' => trans('common.phone_number'),
            'sender_email' => trans('common.email'),
        ];
    }
}
