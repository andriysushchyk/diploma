<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SchoolApplications\StoreSchoolApplicationRequest;
use App\Models\City;
use App\Models\OrganizationType;
use App\Models\Region;
use App\Models\SchoolApplication;
use Illuminate\Support\Facades\Response;

class SchoolApplicationController extends Controller
{
    /**
     * Save application in storage
     * @param StoreSchoolApplicationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSchoolApplicationRequest $request)
    {
        $schoolApplication = SchoolApplication::create($request->getApplicationData());

        return Response::successJson([
            'application' => $schoolApplication
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('guest.school_applications.show');
    }
}
