<?php

namespace App\Http\Controllers\School;

use App\Http\Requests\School\StoreNewsRequest;
use App\Models\NewsItem;
use App\Services\Base64ImageSaver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class NewsController extends Controller
{
    public function create()
    {
        return view('school.news.create');
    }

    /**
     * @param StoreNewsRequest $request
     * @param Base64ImageSaver $base64ImageSaver
     * @return mixed
     */
    public function store(StoreNewsRequest $request, Base64ImageSaver $base64ImageSaver)
    {
        NewsItem::create(array_merge($request->getData(), [
            'school_id' => $request->user()->schools->first()->id,
            'image_path' => $base64ImageSaver->save($request->getImage())
        ]));

        return Response::successJson();
    }

    public function index(Request $request)
    {
        $school = $request->user()->schools->first();
        return view('school.news.index', [
            'news' => $school->news()->orderBy('created_at', "DESC")->paginate(15),
            'school' => $school
        ]);
    }

    public function show(NewsItem $newsItem)
    {
        return view('school.news.show', [
            'newsItem' => $newsItem
        ]);
    }

    public function destroy(NewsItem $newsItem)
    {
        $newsItem->delete();
        return Response::successJson();
    }
}
