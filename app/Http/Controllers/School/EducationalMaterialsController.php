<?php

namespace App\Http\Controllers\School;

use App\Http\Requests\School\StoreEducationMaterialRequest;
use App\Models\EducationMaterial;
use App\Models\GroupSubject;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class EducationalMaterialsController extends Controller
{
    public function index(GroupSubject $groupSubject)
    {
        $materials = EducationMaterial::query()
                                      ->where('group_subject_id', $groupSubject->id)
                                      ->get()
                                      ->map(function (EducationMaterial $educationMaterial) {
                                          $teacher = User::find($educationMaterial->teacher_id);
                                          $name = $teacher->last_name
                                              . " "
                                              . mb_substr($teacher->first_name, 0, 1)
                                              . '.'
                                              . mb_substr($teacher->middle_name, 0, 1)
                                              . '.';

                                          return [
                                              'id'       => $educationMaterial->id,
                                              'title'    => $educationMaterial->title,
                                              'added_by' => $name,
                                              'date'     => $educationMaterial->created_at->format('d.m'),
                                              'format'   => $educationMaterial->extension
                                                  ? $educationMaterial->extension
                                                  : ($educationMaterial->path_to_file
                                                      ? 'Unknown'
                                                      : '-'
                                                  )
                                          ];
                                      });

        return view('school.ed_materials.index', [
            'groupSubject' => $groupSubject->load('group'),
            'materials'    => $materials
        ]);
    }

    public function show(EducationMaterial $educationMaterial)
    {
        return view('school.ed_materials.show', [
            'educationMaterial' => $educationMaterial
        ]);
    }

    public function store(StoreEducationMaterialRequest $request, GroupSubject $groupSubject)
    {

        if ($request->file('file')) {
            ini_set('max_file_uploads', '-1');
            ini_set('upload_max_filesize', '-1');
            ini_set('upload_max_filesize', '-1');
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/files/'), $filename);
        }

        $material = new EducationMaterial([
            'title'            => $request->input('title'),
            'description'      => $request->input('description'),
            'teacher_id'       => $request->user()->id,
            'group_subject_id' => $groupSubject->id,
            'filename'         => '',
        ]);

        if (isset($file)) {
            $material->path_to_file = 'files/' . $filename;
            $material->filename = $file->getClientOriginalName();
            $material->extension = $file->getClientOriginalExtension();
        }

        $material->save();

        return Response::successJson([]);
    }

    public function download(EducationMaterial $educationMaterial)
    {
        return response()->download(
            storage_path('app/public/' . $educationMaterial->path_to_file),
            $educationMaterial->filename
        );
    }

    public function destroy(EducationMaterial $educationMaterial)
    {
    }
}
