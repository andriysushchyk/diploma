<?php

namespace App\Http\Controllers\School\Api;

use App\Http\Requests\School\StoreMarkRequest;
use App\Models\Mark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class MarkController extends Controller
{
    /**
     * @param StoreMarkRequest $storeMarkRequest
     * @return
     */
    public function store(StoreMarkRequest $storeMarkRequest)
    {
        $mark = Mark::create($storeMarkRequest->getData());
        return Response::successJson([
            'mark' => $mark
        ]);
    }

    public function destroy(Mark $mark)
    {
        $mark->delete();
        return Response::successJson();
    }
}
