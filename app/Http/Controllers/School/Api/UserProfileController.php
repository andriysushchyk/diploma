<?php

namespace App\Http\Controllers\School\Api;

use App\Http\Requests\School\UpdateUserProfileRequest;
use App\Services\Base64ImageSaver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends Controller
{
    public function update(UpdateUserProfileRequest $request, Base64ImageSaver $base64ImageSaver)
    {
        $user = $request->user();

        if ($request->hasAvatar()) {
            $user->avatar_path = $base64ImageSaver->save($request->getAvatar());
        }
        $user->login = $request->getLogin();
        $user->save();
        return Response::successJson();
    }
}
