<?php

namespace App\Http\Controllers\School\Api;

use App\Http\Requests\School\UpdatePasswordRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class UserPasswordController extends Controller
{
    public function update(UpdatePasswordRequest $request)
    {
        $user = $request->user();
        if (! password_verify($request->getOldPassword(), $user->password)) {
            return Response::failedJson('', [
                'errors' => [
                    'password' => 'Старий пароль невірний'
                ]
            ], 422);
        }

        $user->password = password_hash($request->getNewPassword(), PASSWORD_BCRYPT);
        $user->save();
        return Response::successJson();
    }
}
