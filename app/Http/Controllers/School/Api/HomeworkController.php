<?php

namespace App\Http\Controllers\School\Api;

use App\Http\Requests\School\UpdateHomeworkRequest;
use App\Models\Homework;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class HomeworkController extends Controller
{
    public function update(UpdateHomeworkRequest $request)
    {
        Homework::query()
                ->where('date', $request->input('date'))
                ->where('group_subject_id', $request->input('group_subject_id'))
                ->delete()
        ;

        Homework::create($request->getData());

        return Response::successJson();
    }
}
