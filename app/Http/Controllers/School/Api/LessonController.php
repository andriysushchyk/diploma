<?php

namespace App\Http\Controllers\School\Api;

use App\Http\Requests\Lessons\DeleteLessonRequest;
use App\Http\Requests\School\StoreAdditionalLessonRequest;
use App\Models\AdditionalLesson;
use App\Models\DeletedLesson;
use App\Models\Lesson;
use App\Models\LessonType;
use App\Services\LessonTypeManager;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class LessonController extends Controller
{
    public function store(StoreAdditionalLessonRequest $request, LessonTypeManager $lessonTypeManager)
    {
        $lesson = AdditionalLesson::create($request->getData());
        $lessonType = new LessonType(
            $lesson->lesson_type,
            $lessonTypeManager->getTitle($lesson->lesson_type)->getTitle()
        );
        $lesson = json_decode(json_encode(
            new Lesson(Carbon::parse($lesson->date), $lessonType, null, $lesson->id)
        ));
        return Response::successJson([
            'lesson' => $lesson
        ]);
    }

    public function destroy(DeleteLessonRequest $request)
    {
        if ($additionalLessonID = $request->getAdditionalLessonID()) {
            AdditionalLesson::find($additionalLessonID)->delete();

        } else {
            DeletedLesson::query()->create([
                'date'              => $request->getDate(),
                'timetable_item_id' => $request->getTimetableItemID(),
            ]);
        }

        return Response::successJson();
    }
}
