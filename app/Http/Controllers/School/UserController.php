<?php

namespace App\Http\Controllers\School;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function show($userLogin)
    {
        $user = User::where('login', $userLogin)->firstOrFail();
        return view('school.profile.user', [
            'user' => $user
        ]);
    }
}
