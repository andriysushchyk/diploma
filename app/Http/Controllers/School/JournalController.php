<?php

namespace App\Http\Controllers\School;

use App\Models\GroupSubject;
use App\Models\Homework;
use App\Models\Lesson;
use App\Models\LessonType;
use App\Services\HomeworkManager;
use App\Services\JournalColumnsPaginator;
use App\Services\JournalManager;
use App\Services\LessonTypeManager;
use App\Services\UkranianCalendarResolver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JournalController extends Controller
{
    /**
     * @param Request $request
     * @param GroupSubject $groupSubject
     * @param JournalManager $journalManager
     * @param UkranianCalendarResolver $ukranianCalendarResolver
     * @param LessonTypeManager $lessonTypeManager
     * @param HomeworkManager $homeworkManager
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(
        Request $request,
        GroupSubject $groupSubject,
        JournalManager $journalManager,
        UkranianCalendarResolver $ukranianCalendarResolver,
        LessonTypeManager $lessonTypeManager,
        HomeworkManager $homeworkManager
    ) {
        $users = $groupSubject->group->users()->orderBy('last_name')->orderBy('first_name')->get();
        $lessonsPaginator = new JournalColumnsPaginator(
            $journalManager->getJournalColumns($groupSubject),
            (int)$request->input('page')
        );

        $from = $lessonsPaginator->dateStart();
        $to = $lessonsPaginator->dateEnd();

        $lessonTypes = $lessonTypeManager->getAll()->map(function (LessonType $lessonType) {
            return [
                'value' => $lessonType->getValue(),
                'label' => $lessonType->getTitle()
            ];
        });

        $marks = $journalManager->getMarks($groupSubject);

        $homeworks = $lessonsPaginator->currentPageItems()
            ->filter(function (Lesson $lesson) {
                return $lesson->timetableItemID;
            })->map(function (Lesson $lesson) use ($homeworkManager) {
                return [
                    'date' => $lesson->date->toDateString(),
                    'date_formatted' => $lesson->date->format('d.m'),
                    'text' => $homeworkManager->getHomeworkContent($lesson->timetableItemID, $lesson->date)
                ];
            })->unique('date')->values();

        return view('school.journal.index', [
            'users'            => $users,
            'subject'          => $groupSubject,
            'lessonsPaginator' => $lessonsPaginator,
            'fromDay'          => $from->day ?? null,
            'fromMonth'        => $ukranianCalendarResolver->getMonthName($from->month ?? 9),
            'toDay'            => $to->day ?? null,
            'toMonth'          => $ukranianCalendarResolver->getMonthName($to->month ?? 5),
            'lessonTypes'      => $lessonTypes,
            'marks'            => $marks,
            'homeworks'        => $homeworks
        ]);
    }
}
