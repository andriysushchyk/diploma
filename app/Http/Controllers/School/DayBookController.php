<?php

namespace App\Http\Controllers\School;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Homework;
use App\Models\School;
use App\Models\TimetableItem;
use App\Models\User;
use App\Services\DailyMarksService;
use App\Services\DatesInDaybook;
use App\Services\UkranianCalendarResolver;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DayBookController extends Controller
{
    public function index(
        Request $request,
        User $user = null,
        DatesInDaybook $datesInDaybook,
        UkranianCalendarResolver $ukrCalendarResolver,
        DailyMarksService $dailyMarks
    ) {
        try {
            $dateInRequest = Carbon::parse($request->input('date'));

        } catch (\Exception $e) {
            $dateInRequest = Carbon::now();
        }

        $user = $user ?? $request->user();
        $school = $user->schools->first();
        $startOfCurrentWeek = $datesInDaybook->getStartOfCurrentStudyWeek($school, $dateInRequest);
        $startOfNextWeek = $datesInDaybook->getStartOfNextStudyWeek($school, $dateInRequest);
        $startOfPrevWeek = $datesInDaybook->getStartOfPreviousStudyWeek($school, $dateInRequest);
        $daysInWeek = $school->getSettingsForCurrentYear()->days_in_week;
        $group = Group::whereHas('users', function ($q) use ($user) {
            $q->where('users.id', $user->id);
        })->first();

        if (!$startOfCurrentWeek) {
            abort(404);
        }

        $days = collect(range(0, $daysInWeek - 1))->map(function ($dayNumber) use (
            $startOfCurrentWeek,
            $ukrCalendarResolver,
            $group,
            $dailyMarks,
            $user
        ) {
            $carbonDate = $startOfCurrentWeek->copy()->addDays($dayNumber);
            $marks = $dailyMarks->getMarks($carbonDate, $user);
            return [
                'month'    => $ukrCalendarResolver->getMonthName($carbonDate->month),
                'dayname'  => $ukrCalendarResolver->getWeekdayTitle($carbonDate->dayOfWeek),
                'day'      => $carbonDate->day,
                'subjects' => $group->timetableItems->where('day_number', $dayNumber + 1)->map(function ($item) use (
                    $marks,
                    $carbonDate
                ) {
                    return [
                        'number' => $item->subject_number,
                        'title' => $item->groupSubject->title,
                        'homework' => $this->getHomework($item, $carbonDate),
                        'marks' => $marks->where('subject_number', $item->subject_number)->toArray()
                    ];
                })->values()
            ];
        });

        $endOfCurrentWeek = $startOfCurrentWeek->copy()->addDays($daysInWeek);

        return view('school.daybook.index', [
            'daybookData' => [
                'from' => [
                    'day'   => $startOfCurrentWeek->day,
                    'month' => $ukrCalendarResolver->getMonthName($startOfCurrentWeek->month)
                ],

                'to' => [
                    'day'   => $endOfCurrentWeek->day,
                    'month' => $ukrCalendarResolver->getMonthName($endOfCurrentWeek->month)
                ],

                'prevUrl' => $startOfPrevWeek === null
                    ? null :
                    route('school.daybook.index', ['user' => $user, 'date' => $startOfPrevWeek->toDateString()]),

                'nextUrl' => $startOfNextWeek === null
                    ? null
                    : route('school.daybook.index', ['user' => $user, 'date' => $startOfNextWeek->toDateString()]),


                'days' => $days
            ],
            'user' => $user
        ]);
    }

    private function getHomework(TimetableItem $timetableItem, Carbon $date)
    {
        if ($timetableItem->groupSubject->timetableItems->contains(function ($item) use ($timetableItem) {
            return $timetableItem->day_number == $item->day_number
                && $timetableItem->subject_number > $item->subject_number;
        })) {
            return '';
        };

        $homework = Homework::query()
                       ->where('group_subject_id', $timetableItem->group_subject_id)
                       ->where('date', $date->toDateString())
                       ->first();

        return $homework
            ? $homework->work
            : ''
        ;
    }
}
