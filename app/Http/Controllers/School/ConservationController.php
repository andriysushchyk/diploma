<?php

namespace App\Http\Controllers\School;

use App\Http\Requests\School\StoreMessageRequest;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;

class ConservationController extends Controller
{
    public function index(Request $request)
    {
        $userId = $request->user()->id;
        $messages = Message::query()
               ->where('to_id', $userId)
               ->orWhere('from_user_id', $userId)
               ->get()
               ->groupBy(function (Message $message) {
                   return $message->to_id > $message->from_user_id
                       ? $message->to_id . ' ' . $message->from_user_id
                       : $message->from_user_id . ' ' . $message->to_id;
               })->map(function (Collection $messages) use ($request) {
                $lastMessage = $messages->sortByDesc('created_at')->first();
                $userId = $lastMessage->from_user_id;
                if ($userId === $request->user()->id) {
                    $userId = $lastMessage->to_id;
                }
                $user = User::find($userId);
                $lastToMessage = $messages->filter(function (Message $message) use ($request) {
                    return $message->to_id == $request->user()->id;
                })->sortByDesc('created_at')->first();
                $isRead = $lastToMessage->is_read ?? true;
                return [
                        'conservationTitle' => $user->first_name . ' ' . $user->last_name,
                        'conservationType' => 'dialog',
                        'lastMessageAt' => $lastMessage->created_at->format('d.m.y H:i'),
                        'lastMessageText' => $lastMessage->body,
                        'isRead' => (bool) $isRead,
                        'user_id' => $user->id,
                        'user_avatar_url' => $user->avatar_url
                    ];
               })->sortByDesc('lastMessageAt')->values();

        return view('school.conservations.index', [
            'messages' => $messages
        ]);
    }

    public function show(Request $request, User $user)
    {
        $messages = Message::query()
                           ->where(function ($q) use ($request, $user) {
                               return $q
                                   ->where('from_user_id', $request->user()->id)
                                   ->where('to_id', $user->id);
                           })
                           ->orWhere(function ($q) use ($request, $user) {
                               return $q
                                   ->where('from_user_id', $user->id)
                                   ->where('to_id', $request->user()->id);
                           })
                           ->get();

        $messages = $messages->map(function (Message $message) use ($request) {
            return $this->transformMessage($message, $request->user());
        });

        return view('school.conservations.show', [
            'messages' => $messages,
            'user' => $user
        ]);
    }

    public function store(StoreMessageRequest $storeMessageRequest)
    {
        $message = Message::create($storeMessageRequest->getData());

        return Response::successJson([
            'message' => $this->transformMessage($message, $storeMessageRequest->user())
        ]);
    }

    public function info(Request $request, User $user)
    {
        $messages = Message::query()
                           ->where(function ($q) use ($request, $user) {
                               return $q
                                   ->where('from_user_id', $user->id)
                                   ->where('to_id', $request->user()->id);
                           })
                           ->get();

        Message::whereIn('id', $messages->pluck('id')->toArray())->update(['is_read' => 1]);
        $messages = $messages->map(function (Message $message) use ($request) {
            return $this->transformMessage($message, $request->user());
        });

        return Response::successJson([
            'messages' => $messages,
        ]);
    }

    private function transformMessage(Message $message, User $user)
    {
        return [
            'body' => $message->body,
            'sended_at' => $message->created_at->copy()->addHours(2)->format('d/m/y H:i'),
            'type' => $user->id == $message->from_user_id ? 'from' : 'to',
            'id' => $message->id
        ];
    }
}
