<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Role;
use App\Models\TimetableItem;
use App\Services\DefaultSubjectsInGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupTimetableController extends Controller
{
    public function edit(Request $request, Group $group, DefaultSubjectsInGroup $defaultSubjectsInGroup)
    {
        $subjects = $group->subjects;

        if (!$subjects->count()) {
            $subjects = $defaultSubjectsInGroup
                ->get($group->year)
                ->map(function ($subjectName) use ($group) {
                    return GroupSubject::query()->create([
                        'group_id' => $group->id,
                        'title'    => $subjectName,
                    ]);
                });
        }

        $teachers = $request->user()->schoolManaged()->users()->where('role', Role::TEACHER)->get();

        $timetable = $group->timetableItems;

        return view('school_management.timetable.edit', [
            'teachers' => $teachers,
            'subjects' => $subjects,
            'group'    => $group,
            'timetable' => $timetable
        ]);
    }
}
