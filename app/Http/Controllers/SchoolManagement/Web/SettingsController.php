<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        $school = $request->user()->schoolManaged();
        return view('school_management.settings', [
            'settings' => $school->getSettingsForCurrentYear()
        ]);
    }
}
