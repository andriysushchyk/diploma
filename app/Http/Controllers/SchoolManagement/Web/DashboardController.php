<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use App\Models\Role;
use App\Services\AbilityToActivateSchoolChecker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(Request $request, AbilityToActivateSchoolChecker $schoolChecker)
    {
        $notPassedRules = $schoolChecker->getNotPassedRules($request->user()->schoolManaged());
        return view('school_management.dashboard', [
            'notPassedRules' => $notPassedRules,
            'school' => $request->user()->schoolManaged()
        ]);
    }

    public function activate(Request $request)
    {
        $school = $request->user()->schoolManaged();
        $settings = $school->getSettingsForCurrentYear();
        $settings->is_active = 1;
        $settings->save();
        return redirect()->back();
    }

    public function usersList(Request $request)
    {
        $path = storage_path('app/public/files/' . time() . '.csv');
        $file = fopen($path, 'w');
        $data = [
            ['ПІБ', 'Роль', 'Логін', 'Пароль'],
        ];

        $school = $request->user()->schoolManaged();
        foreach ($school->users()->where('role', Role::TEACHER)->get() as $teacher) {
            $data[] = [
                "{$teacher->last_name} {$teacher->first_name} {$teacher->middle_name}",
                'Вчитель',
                $teacher->login,
                'password'
            ];
        }

        foreach ($school->groups as $group) {
            foreach ($group->users->sortBy('last_name') as $user) {
                $data[] = [
                    "{$user->last_name} {$user->first_name} {$user->middle_name}",
                    'Учень',
                    $user->login,
                    'password'
                ];

                foreach ($user->parents as $parent) {
                    $data[] = [
                        "{$parent->last_name} {$parent->first_name} {$parent->middle_name}",
                        'Опікун',
                        $parent->login,
                        'password'
                    ];
                }
            }
        }

        foreach ($data as $item) {
            fputcsv($file, $item, ';');
        }
        fclose($file);
        return response()->download($path, 'Користувачі.csv');
    }
}
