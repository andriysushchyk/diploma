<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use App\Models\Group;
use App\Models\School;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    /**
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Group $group)
    {
        return view('school_management.students.index', [
            'students' => $group->users->sortBy('last_name')->values(),
            'group'    => $group
        ]);
    }
}
