<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use App\Models\Role;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        $school = $request->user()->schoolManaged();
        $teachers = $school->users()->where('role', Role::TEACHER)->get();

        return view('school_management.teachers.index', [
            'teachers' => $teachers
        ]);
    }
}
