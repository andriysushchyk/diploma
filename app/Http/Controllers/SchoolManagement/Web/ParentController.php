<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentController extends Controller
{
    public function index(User $user)
    {
        $user->load('parents');
        return view('school_management.parents.index', [
            'user' => $user
        ]);
    }
}
