<?php

namespace App\Http\Controllers\SchoolManagement\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        $groups = $request->user()->schoolManaged()->groups;

        return view('school_management.groups.index', [
            'groups' => $groups
        ]);
    }
}
