<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Requests\SchoolManagement\UpstoreGroupRequest;
use App\Models\Group;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class GroupController extends Controller
{
    /**
     * @param UpstoreGroupRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UpstoreGroupRequest $request)
    {
        $school = $request->user()->schoolManaged();
        $group = Group::create(array_merge($request->getData(), [
            'school_id' => $school->id
        ]));
        return Response::successJson(['item' => $group]);
    }

    /**
     * @param Group $group
     * @param UpstoreGroupRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Group $group, UpstoreGroupRequest $request)
    {
        $group->update($request->getData());
        return Response::successJson(['item' => $group]);
    }

    /**
     * @param Group $group
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $group->delete();
        return Response::successJson([]);
    }
}
