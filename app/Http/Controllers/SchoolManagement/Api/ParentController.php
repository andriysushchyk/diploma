<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Models\Role;
use App\Models\User;
use App\Services\UsernameGenerator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class ParentController extends Controller
{
    public function store(Request $request, User $user, UsernameGenerator $usernameGenerator)
    {
        $this->validate($request, [
            'first_name' => 'required_if:parentType,NEW',
            'middle_name' => 'required_if:parentType,NEW',
            'last_name' => 'required_if:parentType,NEW',
            'parentType' => 'required|in:NEW,EXISTING',
            'login' => 'required_if:parentType,EXISTING'
        ]);

        $parentType = $request->input('parentType');
        if ($parentType === 'NEW') {
            $parent = new User($request->only(['first_name', 'middle_name', 'last_name']));
            $parent->role = Role::PARENT;
            if (!$parent->login) {
                $parent->login = $usernameGenerator->generate($user);
            }
            $parent->password = bcrypt('password');
            $parent->save();
            $request->user()->schoolManaged()->users()->syncWithoutDetaching([$parent->id]);

        } else {
            $this->validate($request, [
                'login' => 'exists:users,login'
            ]);
            $parent = User::where('login', $request->input('login'))->first();
        }

        $user->parents()->syncWithoutDetaching([$parent->id]);

        return Response::successJson([
            'user' => $user
        ]);
    }

    public function update(User $user)
    {
    }
}
