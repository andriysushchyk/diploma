<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Requests\SchoolManagement\Api\UpdateSchoolSettingsRequest;
use App\Models\SchoolSettings;
use App\Models\Year;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class SchoolSettingsController extends Controller
{
    public function update(UpdateSchoolSettingsRequest $request)
    {
        $school = $request->user()->schoolManaged();
        $schoolSettings = $school->settings;

        if ($schoolSettings) {
            $schoolSettings->update($request->getData());

        } else {
            SchoolSettings::create(array_merge($request->getData(), [
               'year_id' => Year::current()->id,
               'school_id' => $school->id,
            ]));
        }

        return Response::successJson();
    }
}
