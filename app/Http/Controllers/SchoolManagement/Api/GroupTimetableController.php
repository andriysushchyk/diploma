<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Requests\SchoolManagement\UpstoreGroupTimetableRequest;
use App\Models\Group;
use App\Models\TimetableItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class GroupTimetableController extends Controller
{
    public function update(UpstoreGroupTimetableRequest $request, Group $group)
    {
        DB::transaction(function () use ($group, $request) {
            $group->timetableItems()->delete();
            $request->getTimetableItems()->groupBy('group_subject_id')->each(function ($subjectItems) {
                $subjectItems->first()->groupSubject->timetableItems()->saveMany($subjectItems);
            });
        });

        return Response::successJson();
    }
}
