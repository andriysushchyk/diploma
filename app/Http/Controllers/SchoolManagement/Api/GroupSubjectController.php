<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Requests\SchoolManagement\UpstoreSubjectInGroupRequest;
use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class GroupSubjectController extends Controller
{
    /**
     * @param UpstoreSubjectInGroupRequest $request
     * @param Group $group
     * @return mixed
     */
    public function store(UpstoreSubjectInGroupRequest $request, Group $group)
    {
        $subjectInGroup = GroupSubject::query()->create(array_merge($request->getData(), [
            'group_id' => $group->id
        ]));

        return Response::successJson([
            'subject' => $subjectInGroup
        ]);
    }

    /**
     * @param UpstoreSubjectInGroupRequest $request
     * @param GroupSubject $groupSubject
     * @return mixed
     */
    public function update(UpstoreSubjectInGroupRequest $request, GroupSubject $groupSubject)
    {
        $groupSubject->update($request->getData());
        return Response::successJson([
            'subject' => $groupSubject
        ]);
    }

    /**
     * @param GroupSubject $groupSubject
     * @return mixed
     * @throws \Exception
     */
    public function destroy(GroupSubject $groupSubject)
    {
        $groupSubject->delete();
        return Response::successJson();
    }
}
