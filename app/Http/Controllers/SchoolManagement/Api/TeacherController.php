<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Requests\SchoolManagement\UpStoreTeacherRequest;
use App\Models\User;
use App\Services\UsernameGenerator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class TeacherController extends Controller
{
    public function store(UpStoreTeacherRequest $request, UsernameGenerator $usernameGenerator)
    {
        $user = new User($request->getData());
        if (!$user->login) {
            $user->login = $usernameGenerator->generate($user);
        }
        $user->password = '';
        $user->save();

        $request->user()->schoolManaged()->users()->syncWithoutDetaching([$user->id]);
        return Response::successJson(['item' => $user]);
    }

    public function update(UpStoreTeacherRequest $request, User $user)
    {
        $user->update($request->getData());
        return Response::successJson(['item' => $user]);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return Response::successJson();
    }
}
