<?php

namespace App\Http\Controllers\SchoolManagement\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SchoolManagement\UpstoreGroupUserRequest;
use App\Models\Group;
use App\Models\User;
use App\Services\UsernameGenerator;
use Illuminate\Support\Facades\Response;

class GroupUserController extends Controller
{
    public function store(UpstoreGroupUserRequest $storeUserRequest, Group $group, UsernameGenerator $usernameGenerator)
    {
        $user = new User($storeUserRequest->getData());
        if (!$user->login) {
            $user->login = $usernameGenerator->generate($user);
        }
        $user->password = '';
        $user->save();

        $group->users()->syncWithoutDetaching([$user->id]);
        $group->school->users()->syncWithoutDetaching([$user->id]);

        return Response::successJson(['item' => $user]);
    }

    public function update(UpstoreGroupUserRequest $request, Group $group, User $user)
    {
        $user->update($request->getData());
        return Response::successJson(['item' => $user]);
    }

    public function destroy(Group $group, User $user)
    {
        $user->delete();
        return Response::successJson();
    }
}
