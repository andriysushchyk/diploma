<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    /** @var Carbon */
    public $yearStart;

    /** @var Carbon */
    public $yearEnd;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->yearStart = Carbon::createFromDate(2018, 9, 3)->setTime(0, 0, 0, 0);
        $this->yearEnd = Carbon::createFromDate(2019, 5, 31)->setTime(0, 0, 0, 0);
    }

    protected $fillable = [
        'short_name',
        'full_name',
        'city_id',
        'organization_type_id',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'schools_users');
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function settings()
    {
        return $this->hasOne(SchoolSettings::class)
                    ->where('year_id', Year::current()->id);
    }

    public function getSettingsForCurrentYear()
    {
        if ($this->settings) {
            return $this->settings;
        }

        $settings = Year::current()->settings();
        $settings->school_id = $this->id;
        $settings->save();
        return $settings;
    }

    public function news()
    {
        return $this->hasMany(NewsItem::class);
    }
}
