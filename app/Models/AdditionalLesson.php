<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalLesson extends Model
{
    protected $fillable = [
        'group_subject_id',
        'lesson_type',
        'date'
    ];
}
