<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $guarded = [];

    protected $attributes = [
        'breaks' => '[]'
    ];

    protected $casts = [
        'breaks' => 'array'
    ];

    public static function current()
    {
        $year = self::query()->where('is_active', 1)->orderByDesc('id')->first();
        if ($year) {
            return $year;
        }

        $now = Carbon::now();
        $title = $now->month < 8
            ? (($now->year - 1) . "/" . $now->year)
            : (($now->year) . "/" . ($now->year + 1))
        ;
        return Year::create([
           'title' => $title,
           'is_active' => 1,
        ]);
    }

    public function settings()
    {
        return new SchoolSettings([
            'year_id' => $this->id,
           'breaks' => $this->breaks,
           'days_in_week' => 5
        ]);
    }
}
