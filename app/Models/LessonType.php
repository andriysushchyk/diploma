<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LessonType implements \JsonSerializable
{
    protected $value;

    protected $title;

    public function __construct($value, $title)
    {
        $this->value = $value;

        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'value' => $this->value,
            'title' => $this->title
        ];
    }
}
