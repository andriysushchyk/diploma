<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'body',
        'to_id',
        'from_user_id',
        'to_type'
    ];
}
