<?php

namespace App\Models;

use App\Scopes\CurrentYearScope;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        $this->attributes = [
            'year_id' => Year::current()->id
        ];

        parent::__construct($attributes);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CurrentYearScope);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'groups_users');
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function subjects()
    {
        return $this->hasMany(GroupSubject::class);
    }

    public function timetableItems()
    {
        return $this->hasManyThrough(TimetableItem::class, GroupSubject::class, 'group_id', 'group_subject_id');
    }
}
