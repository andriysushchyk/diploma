<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class NewsItem extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'title',
        'image_path',
        'full_text',
        'preview_text',
        'school_id'
    ];

    protected $appends = [
        'image_url'
    ];

    protected $hidden = [
        'image_path'
    ];

    public function getImageUrlAttribute()
    {
        return Storage::disk('public')->url($this->image_path);
    }
}
