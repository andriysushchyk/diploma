<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimetableItem extends Model
{
    protected $fillable = [
        'group_subject_id',
        'subject_number',
        'day_number',
    ];

    public function groupSubject()
    {
        return $this->belongsTo(GroupSubject::class, 'group_subject_id');
    }
}
