<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarkValue extends Model
{
    protected $table = 'mark_values';

    protected $fillable = [
        'title'
    ];
}
