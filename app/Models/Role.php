<?php

namespace App\Models;

class Role
{
    const STUDENT = 0;

    const PARENT = 1;

    const TEACHER = 2;
}
