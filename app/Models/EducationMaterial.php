<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationMaterial extends Model
{
    protected $fillable = [
        'teacher_id',
        'group_subject_id',
        'title',
        'path_to_file',
        'description',
        'filename',
        'extension'
    ];
}
