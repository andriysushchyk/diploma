<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'password',
        'school_id',
        'first_name',
        'last_name',
        'middle_name',
        'role',
        'avatar_path'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'avatar_url'
    ];

    protected $attributes = [
        'avatar_path' => 'images/default.png'
    ];

    public function schools()
    {
        return $this->belongsToMany(School::class, 'schools_users')
                    ->withPivot('permissions');
    }

    /**
     * @return School
     */
    public function schoolManaged()
    {
        return $this->schools->first(function (School $school) {
            return $school->pivot->permissions;
        });
    }

    public function getAvatarUrlAttribute()
    {
        return Storage::disk('public')->url($this->avatar_path);
    }

    public function groupOfStudent()
    {
        return $this
            ->hasOne(Group::class);
    }

    public function canManageNews(School $school)
    {
        return $school
            ->users()
            ->wherePivot('permissions', 1)
            ->where('user_id', $this->id)
            ->count();
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'groups_users');
    }



    public function groupSubjects()
    {
        return $this->hasMany(GroupSubject::class, 'teacher_id');
    }

    public function parents()
    {
        return $this->belongsToMany(User::class, 'parents_students', 'student_id', 'parent_id');
    }

    public function childs()
    {
        return $this->belongsToMany(User::class, 'parents_students', 'parent_id', 'student_id');
    }

    public function messagesCount()
    {
        $userId = $this->id;

        $count = Message::query()
                           ->where('to_id', $userId)
                           ->orWhere('from_user_id', $userId)
                           ->get()
                           ->groupBy(function (Message $message) {
                               return $message->to_id > $message->from_user_id
                                   ? $message->to_id . ' ' . $message->from_user_id
                                   : $message->from_user_id . ' ' . $message->to_id;
                           })->map(function ($messages) {
                            $lastMessage = $messages->sortByDesc('created_at')->first();
                            $userId = $lastMessage->from_user_id;
                            if ($userId === $this->id) {
                                $userId = $lastMessage->to_id;
                            }
                            $user = User::find($userId);
                            $lastToMessage = $messages->filter(function (Message $message) {
                                return $message->to_id == $this->id;
                            })->sortByDesc('created_at')->first();
                            $isRead = $lastToMessage->is_read ?? true;
                            return [
                            'conservationTitle' => $user->first_name . ' ' . $user->last_name,
                            'conservationType' => 'dialog',
                            'lastMessageAt' => $lastMessage->created_at->format('d.m.y H:i'),
                            'lastMessageText' => $lastMessage->body,
                            'isRead' => (bool) $isRead,
                            'user_id' => $user->id,
                            'user_avatar_url' => $user->avatar_url
                            ];
                           })->filter(function ($data) {
                            return !$data['isRead'];
                           })->count();

        return $count;
    }
}
