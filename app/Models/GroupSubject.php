<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupSubject extends Model
{
    protected $table = 'groups_subjects';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'group_id',
        'teacher_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function timetableItems()
    {
        return $this->hasMany(TimetableItem::class);
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function additionalLessons()
    {
        return $this->hasMany(AdditionalLesson::class);
    }
}
