<?php

namespace App\Models;

use Carbon\Carbon;

class Lesson implements \JsonSerializable
{
    public $date;

    public $lessonType;

    public $additionalLessonID;

    public $timetableItemID;

    public function __construct(Carbon $date, LessonType $lessonType, $timetableItemID, $additionalLessonID)
    {
        $this->date = $date;
        $this->lessonType = $lessonType;
        $this->additionalLessonID = $additionalLessonID;
        $this->timetableItemID = $timetableItemID;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'date' => $this->date->toDateTimeString(),
            'lessonType' => [
                'value' => $this->lessonType->getValue(),
                'label' => $this->lessonType->getTitle()
            ],
            'additionalLessonId' => $this->additionalLessonID,
            'timetableItemId' => $this->timetableItemID,
        ];
    }
}
