<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SchoolSettings extends Model
{
    protected $table = 'school_year_settings';

    public $timestamps = false;

    protected $fillable = [
        'year_id',
        'school_id',
        'breaks',
        'days_in_week',
    ];

    protected $casts = [
        'breaks' => 'array'
    ];

    /**
     * @return $this|Model
     */
    public static function defaultSettings()
    {
        $currentYear = Year::current();
        return self::create([
            'year_id' => $currentYear->id,
            'breaks' => $currentYear->breaks,
            'days_in_week' => 5
        ]);
    }

    public function getBreaks()
    {
        return collect($this->breaks)->map(function ($break) {
            return new YearBreak(
                Carbon::parse($break['from']),
                Carbon::parse($break['to']),
                $break['reason']
            );
        });
    }
}
