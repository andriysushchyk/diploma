<?php

namespace App\Models;

use App\Services\LessonTypeManager;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    protected $fillable = [
        'mark_value_id',
        'mark_lesson_type',
        'mark_lesson_id',
        'user_id',
        'date'
    ];

    protected $appends = [
        'mark_type'
    ];

    public function getMarkTypeAttribute()
    {
        if ($this->markLesson instanceof TimetableItem) {
            return new LessonType(LessonTypeManager::POTOCHNA, 'Поточна');
        }

        return new LessonType(
            $this->markLesson->lesson_type,
            (new LessonTypeManager())->getTitle($this->markLesson->lesson_type)
        );
    }

    public function markLesson()
    {
        return $this->morphTo();
    }

    public function markValue()
    {
        return $this->belongsTo(MarkValue::class);
    }
}
