<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityType extends Model
{
    public $timestamps = false;

    protected $fillable = ['title'];
}
