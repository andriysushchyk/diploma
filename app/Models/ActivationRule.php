<?php

namespace App\Models;

class ActivationRule
{
    /** @var string */
    protected $notPassedText;

    /** @var string */
    protected $link;

    /** @var bool */
    protected $isPassed;

    public function __construct(string $text, string $link, bool $isPassed)
    {
        $this->notPassedText = $text;

        $this->link = $link;

        $this->isPassed = $isPassed;
    }

    /**
     * @return bool
     */
    public function getIsPassed()
    {
        return $this->isPassed;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getNotPassedText()
    {
        return $this->notPassedText;
    }
}
