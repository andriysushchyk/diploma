<?php

namespace App\Models;

use Carbon\Carbon;

class YearBreak
{
    public $from;

    public $to;

    public $reason;

    public function __construct(Carbon $dateFrom, Carbon $dateTo, string $reason)
    {
        $this->from = $dateFrom;
        $this->to = $dateTo;
        $this->reason = $reason;
    }
}
