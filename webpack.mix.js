const WebpackShellPlugin = require('webpack-shell-plugin');
let mix = require('laravel-mix');

// Add shell command plugin configured to create JavaScript language file
mix.webpackConfig({
    plugins:
        [
            new WebpackShellPlugin({onBuildStart:['php artisan lang:js --quiet'], onBuildEnd:[]})
        ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/School/app.js', 'public/js/school.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sourceMaps();
