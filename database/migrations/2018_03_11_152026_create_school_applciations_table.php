<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolApplciationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_short_name')->default('');
            $table->text('school_full_name');
            $table->text('school_phone_number');

            $table->unsignedInteger('school_city_id');
            $table->unsignedInteger('school_organization_type_id');

            $table->string('sender_first_name')->default('');
            $table->string('sender_middle_name')->default('');
            $table->string('sender_last_name')->default('');
            $table->string('sender_position')->default('');
            $table->string('sender_phone_number')->default('');
            $table->string('sender_email')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_applications');
    }
}
