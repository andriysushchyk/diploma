<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetableItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetable_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_subject_id');
            $table->unsignedInteger('day_number');
            $table->unsignedInteger('subject_number');
            $table->timestamps();

            $table->foreign('group_subject_id')
                ->references('id')
                ->on('groups_subjects')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetable_items');
    }
}
