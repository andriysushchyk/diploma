<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolYearSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_year_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('school_id');
            $table->unsignedInteger('days_in_week');
            $table->boolean('is_active')->default(0);
            $table->json('breaks');

            $table->foreign('year_id')
                ->references('id')
                ->on('years')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE')
            ;

            $table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE')
            ;


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_year_settings');
    }
}
