<?php

use App\Models\City;
use App\Models\CityType;
use App\Models\Region;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    protected $locationTypes = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedLocationTypes();
        $locationCSVLines = array_map('str_getcsv', file(base_path('database/data/locations.csv')));

        /** @var Region $currentRegion */
        $currentRegion = null;

        /** @var array $citiesOfCurrentRegion */
        $citiesOfCurrentRegion = [];

        $capitalRegion = Region::create(['name' => 'Київ']);
        City::create(['name' => 'Київ', 'region_id' => $capitalRegion->id, 'type_id' => $this->locationTypes['М']->id]);

        foreach ($locationCSVLines as $locationLine) {

            $locationStr = $locationLine[2];
            $isRegion = $this->isRegion($locationStr);

            if ($isRegion && $currentRegion) {
                City::insert($citiesOfCurrentRegion);
                $citiesOfCurrentRegion = [];
                $currentRegion = $this->createRegion($locationStr);

            } elseif ($isRegion) {
                $currentRegion = $this->createRegion($locationStr);

            } elseif ($this->isCity($locationLine)) {
                $citiesOfCurrentRegion[] = $this->cityData($locationLine, $currentRegion->id);
            }
        }
    }

    private function seedLocationTypes()
    {
        $this->locationTypes = [
            'М' => CityType::create(['title' => 'Місто']),
            'Т' => CityType::create(['title' => 'Селище міського типу']),
            'С' => CityType::create(['title' => 'Ceло']),
            'Щ' => CityType::create(['title' => 'Селище']),
        ];
    }

    private function cityData($locationLine, $regionID)
    {
        return [
            'name'      => mb_convert_case($locationLine[2], MB_CASE_TITLE),
            'type_id'   => $this->locationTypes[$locationLine[1]]->id,
            'region_id' => $regionID
        ];
    }

    private function isCity($locationLine)
    {
        return in_array($locationLine[1], array_keys($this->locationTypes));
    }

    private function createRegion($regionStr)
    {
        $regionStr = mb_split('/', $regionStr)[0];
        $regionStr = mb_convert_case(trim(mb_split('ОБЛАСТЬ', $regionStr)[0]), MB_CASE_TITLE);
        return Region::create(['name' => $regionStr]);
    }

    private function isRegion($location)
    {
        return str_contains($location, ['ОБЛАСТЬ', 'АВТОНОМНА']);
    }
}
