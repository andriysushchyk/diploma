<?php

use App\Models\MarkValue;
use Illuminate\Database\Seeder;

class MarkValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => '1'],
            ['title' => '2'],
            ['title' => '3'],
            ['title' => '4'],
            ['title' => '5'],
            ['title' => '6'],
            ['title' => '7'],
            ['title' => '8'],
            ['title' => '9'],
            ['title' => '10'],
            ['title' => '11'],
            ['title' => '12'],
            ['title' => 'н'],
            ['title' => 'н/а'],
            ['title' => 'зар.'],
            ['title' => 'зв.']
        ];

        foreach ($data as $dataItem) {
            MarkValue::firstOrCreate($dataItem);
        }
    }
}
