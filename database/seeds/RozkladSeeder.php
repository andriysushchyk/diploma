<?php

use App\Models\Group;
use App\Models\GroupSubject;
use Illuminate\Database\Seeder;

class RozkladSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = \App\Models\Group::whereTitle('5-А')->first();
        GroupSubject::query()->delete();
        $ukrMovaLit = $this->createGroupSubjects($group, ['Українська мова', 'Українська література']);
        $angl =$this->createGroupSubjects($group, ['Англійська мова']);
        $zar_literatura = $this->createGroupSubjects($group, ['Світова література']);
        $history = $this->createGroupSubjects($group, ['Історія України']);
        $math = $this->createGroupSubjects($group, ['Математика']);
        $priroda = $this->createGroupSubjects($group, ['Природознавство']);
        $informa = $this->createGroupSubjects($group, ['Інформатика']);
        $osnoviZdor = $this->createGroupSubjects($group, ['Основи здоров\'я']);
        $muzika = $this->createGroupSubjects($group, ['Музичне мистецтво']);
        $obraz = $this->createGroupSubjects($group, ['Образотворче мистецтво']);
        $trudove = $this->createGroupSubjects($group, ['Трудове навчання']);
        $fizra = $this->createGroupSubjects($group, ['Фізкультура']);

        $timetableItems = [

            $this->createTimetableItem($ukrMovaLit[0], 1, 1),
            $this->createTimetableItem($ukrMovaLit[1], 1, 2),
            $this->createTimetableItem($angl[0], 1, 3),
            $this->createTimetableItem($priroda[0], 1, 4),
            $this->createTimetableItem($osnoviZdor[0], 1, 5),
            $this->createTimetableItem($fizra[0], 1, 5),

            $this->createTimetableItem($ukrMovaLit[1], 2, 1),
            $this->createTimetableItem($math[0], 2, 2),
            $this->createTimetableItem($zar_literatura[0], 2, 3),
            $this->createTimetableItem($informa[0], 2, 4),
            $this->createTimetableItem($obraz[0], 2, 5),
            $this->createTimetableItem($osnoviZdor[0], 2, 6),

            $this->createTimetableItem($history[0], 3, 1),
            $this->createTimetableItem($math[0], 3, 2),
            $this->createTimetableItem($angl[0], 3, 3),
            $this->createTimetableItem($priroda[0], 3, 4),
            $this->createTimetableItem($muzika[0], 3, 5),
            $this->createTimetableItem($fizra[0], 3, 6),

            $this->createTimetableItem($ukrMovaLit[0], 4, 1),
            $this->createTimetableItem($math[0], 4, 2),
            $this->createTimetableItem($angl[0], 4, 3),
            $this->createTimetableItem($history[0], 4, 4),
            $this->createTimetableItem($informa[0], 4, 5),
            $this->createTimetableItem($trudove[0], 4, 6),

            $this->createTimetableItem($ukrMovaLit[0], 5, 1),
            $this->createTimetableItem($informa[0], 5, 2),
            $this->createTimetableItem($zar_literatura[0], 5, 3),
            $this->createTimetableItem($muzika[0], 5, 4),
            $this->createTimetableItem($fizra[0], 5, 5),
        ];

    }

    protected function createTimetableItem($groupSubject, $dayNumber, $subjectNumber)
    {
        return \App\Models\TimetableItem::create([
            'group_subject_id' => $groupSubject->id,
            'day_number'       => $dayNumber,
            'subject_number'   => $subjectNumber
        ]);
    }

    protected function createGroupSubjects(Group $group, $groupSubjectTitles)
    {
        $teacher = create(\App\Models\User::class, [
            'role' => \App\Models\Role::TEACHER
        ]);
        if ($groupSubjectTitles[0] == 'Математика') {
            $teacher->last_name = 'Ковальчук';
            $teacher->first_name = 'Наталія';
            $teacher->middle_name = 'Іванівна';
            $teacher->login = (new \App\Services\UsernameGenerator())->generate($teacher);
            $teacher->save();
        }

        $group->school->users()->syncWithoutDetaching([$teacher->id]);

        $groupSubjects = [];
        foreach ($groupSubjectTitles as $title) {
            $groupSubjects[] = GroupSubject::create([
                'group_id' => $group->id,
                'title' => $title,
                'teacher_id' => $teacher->id
            ]);
        }

        return $groupSubjects;
    }
}
