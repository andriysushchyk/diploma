<?php

use App\Models\City;
use App\Models\Group;
use App\Models\OrganizationType;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoSeeder extends Seeder
{
    /* @var Faker\Generator */
    protected $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create('uk_UA');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createYear();

        if (!\App\Models\MarkValue::count()) {
            $this->call(MarkValuesSeeder::class);
        }

        if (!City::count()) {
            $this->call(LocationSeeder::class);
        }

        if (!OrganizationType::count()) {
            $this->call(SchoolOrganizationTypeSeeder::class);
        }

        $school = factory(School::class)->create();

        //Create admin of school:
        $schoolAdmin = factory(User::class)->create([
            'role' => Role::TEACHER
        ]);

        $schoolAdmin->schools()->sync([$school->id => ['permissions' => 1]]);

        $groups = [
            5 => ['5-А', '5-Б'],
            6 => ['6-А', '6-Б'],
            7 => ['7-А', '7-Б'],
            8 => ['8'],
            9 => ['9-А', '9-Б', '9-В'],
        ];

        foreach ($groups as $year => $group) {
            foreach ($group as $groupTitle) {
                $group = factory(Group::class)->create([
                    'school_id' => $school->id,
                    'year'      => $year,
                    'title'     => $groupTitle
                ]);
                $this->createUsersForGroup($group);
            }
        }

        $teachers = collect();

        for ($i = 0; $i < 10; $i++) {
            $teachers->push(factory(User::class)->create([
                'role' => Role::TEACHER
            ]));
        }

        $school->users()->syncWithoutDetaching($teachers->pluck('id')->toArray());
//        $this->createNews($school);
//        $this->run(RozkladSeeder::class);
    }

    /**
     * @param Group $group
     */
    private function createUsersForGroup(Group $group)
    {
        $users = create(User::class, [
            'role' => Role::STUDENT
        ], 17);

        $localizedFaker = Faker\Factory::create("uk_UA");

        $parentsIDs = [];
        if ($group->title == '5-А') {
            foreach ($users as $user) {
                $parent1 = make(User::class, [
                    'role'        => Role::PARENT,
                    'last_name'   => $user->last_name,
                    'first_name'  => $localizedFaker->firstNameMale,
                    'middle_name' => $localizedFaker->middleNameMale,
                ]);
                $parent1->login = (new \App\Services\UsernameGenerator())->generate($parent1);
                $parent1->save();
                $parentsIDs[] = $parent1->id;

                $parent2 = make(User::class, [
                    'role'        => Role::PARENT,
                    'last_name'   => $user->last_name,
                    'first_name'  => $localizedFaker->firstNameFemale,
                    'middle_name' => $localizedFaker->middleNameFemale,
                ]);
                $parent2->login = (new \App\Services\UsernameGenerator())->generate($parent2);
                $parent2->save();
                $parentsIDs[] = $parent2->id;

                $user->parents()->syncWithoutDetaching([$parent1->id, $parent2->id]);
            }
            $group->school->users()->syncWithoutDetaching($parentsIDs);
        }

        $group->school->users()->syncWithoutDetaching($users->pluck('id'));
        $group->users()->syncWithoutDetaching($users->pluck('id')->toArray());
    }

    private function createYear()
    {
        \App\Models\Year::query()->create([
            'title'     => '2018/2019',
            'breaks'    => [
                [
                    'from'   => '2017-10-16',
                    'to'     => '2017-10-16',
                    'reason' => 'Осінні канікули'
                ],
                [
                    'from'   => '2017-12-30',
                    'to'     => '2018-01-14',
                    'reason' => 'Зимові канікули'
                ],
                [
                    'from'   => '2018-05-09',
                    'to'     => '2018-05-09',
                    'reason' => 'День Перемоги над фашизмом',
                ],
                [
                    'from'   => '2018-03-08',
                    'to'     => '2018-03-08',
                    'reason' => 'Весняні канікули'
                ],
            ],
            'is_active' => 1,

        ]);
    }

    public function createNews(School $school)
    {
        $textSize = 1800;
        create(\App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'Виховний захід у 3-А класі',
            'image_path'   => 'images/demo1.jpg',
            'preview_text' => 'Відкрита літературна виховна година присвячена відомій поетесі Л.Українці відбулася в 3-А класі.',
            'full_text'    => $this->faker->realText($textSize)
        ]);

        create(\App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'Загальношкільна толока',
            'image_path'   => 'images/demo2.jpg',
            'preview_text' => 'Адміністрація школи дякує усім школярам, учителям, технічним працівникам, хто взяв участь у загальношкільній толоці, яка відбулася в п’ятницю, 13 квітня. 
Учнями, працівниками школи було прибрано й упорядковано територію навколо закладу та сквер поблизу малого ринку.',
            'full_text'    => $this->faker->realText($textSize)
        ]);

        create(App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'З Великоднем!',
            'image_path'   => 'images/demo3_pasha.jpg',
            'preview_text' => 'Дорогі учні, батьки, бабусі та дідусі, друзі нашої школи, шановні колеги, прийміть найщиріші вітання з нагоди найбільшого християнського свята Воскресіння Христового.',
            'full_text'    => $this->faker->realText($textSize)
        ]);

        create(App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'Відбулася атестація вчителів',
            'image_path'   => 'images/demo4_atest.jpg',
            'preview_text' => '27 березня відбувся І (шкільний) етап атестації педагогічних працівників. У 2017-2018 навчальному році атестацію проходили шестеро педагогів школи. ',
            'full_text'    => $this->faker->realText($textSize)
        ]);

        create(App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'Засідання педагогічної ради школи',
            'image_path'   => 'images/demo7_pedrada.jpg',
            'preview_text' => 'У п’ятницю, 23 лютого, у нашому закладі відбулося засідання педагогічної ради школи. Розглядалися питання «Управління школою в сучасних умовах», «Педагогічне новаторство як чинник реформування сучасної освіти»',
            'full_text'    => $this->faker->realText($textSize)
        ]);

        create(\App\Models\NewsItem::class, [
            'school_id'    => $school->id,
            'title'        => 'Увага! Призупинення занять відмінено',
            'image_path'   => 'images/demo6_zannatya.jpg',
            'preview_text' => 'З 5 березня школа працюватиме в звичному режимі, призупинення навчання відмінено. У понеділок для початкових класів початок навчання залишається без змін.',
            'full_text'    => $this->faker->realText($textSize)
        ]);
    }

}
