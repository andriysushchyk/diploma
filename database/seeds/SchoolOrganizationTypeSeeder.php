<?php

use Illuminate\Database\Seeder;

class SchoolOrganizationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
          'Державний заклад',
          'Комунальний заклад',
          'Приватний заклад',
        ];

        foreach ($types as $type) {
            \App\Models\OrganizationType::create([
                'name' => $type
            ]);
        }
    }
}
