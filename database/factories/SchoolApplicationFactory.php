<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\SchoolApplication::class, function (Faker $faker) {
    return [
        'school_short_name' => $faker->name,
        'school_full_name' => $faker->name,
        'school_phone_number' => $faker->phoneNumber,
        'school_city_id' => create(\App\Models\City::class)->id,
        'school_organization_type_id' => create(\App\Models\OrganizationType::class)->id,
        'sender_first_name' => $faker->word,
        'sender_middle_name' => $faker->word,
        'sender_last_name' => $faker->word,
        'sender_phone_number' => $faker->phoneNumber,
        'sender_email' => $faker->email,
    ];
});
