<?php

use App\Models\MarkValue;
use Faker\Generator as Faker;

$factory->define(MarkValue::class, function (Faker $faker) {
    return [
        'title' => $faker->numberBetween(1, 12)
    ];
});
