<?php

//use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\Models\Role;
use App\Models\User;

$localizedFaker =  Faker\Factory::create("uk_UA");

$factory->define(App\Models\User::class, function () use ($localizedFaker) {
    $generateRandomMale = (random_int(1, 100) > 70);
    $lastName = $localizedFaker->lastName;
    $firstName = $generateRandomMale ? $localizedFaker->firstNameMale : $localizedFaker->firstNameFemale;
    $middleName = $generateRandomMale ? $localizedFaker->middleNameMale : $localizedFaker->middleNameFemale;

    return [
        'login'       => (new \App\Services\UsernameGenerator())->generate(new User([
                'first_name'  => $firstName,
                'last_name'   => $lastName,
                'middle_name' => $middleName
            ])),
        'first_name'  => $firstName,
        'last_name'   => $lastName,
        'middle_name' => $middleName,
        'password'    => bcrypt('password'),
        'role'        => Role::TEACHER,
    ];
});
