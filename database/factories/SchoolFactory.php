<?php

use Faker\Generator as Faker;

$fakeSchools = [
    [
        'Володимир-Волинська гімназія',
        'Володимир-Волинська гімназія Володимир-Волинської міської ради Волинської області',
        'Володимир-Волинський'
    ],
    [
        'Краснодонська ЗШ №1',
        'Комунальна установа "Краснодонська загальноосвітня школа І-ІІІ ступенів № 9 Краснодонської міської ради Луганської області"',
        'Краснодон'
    ],
    [
        'Малинська школа-ліцей № 1',
        'Малинський загальноосвітній навчально-виховний комплекс "Школа-ліцей№ 1 ім. Ніни Сосніної" І-ІІІ ступенів',
        'Малин'
    ]
];

$factory->define(App\Models\School::class, function (Faker $faker) use ($fakeSchools) {
    $fakeSchoolNumber = App\Models\School::count() % count($fakeSchools);
    return [
        'short_name' => $fakeSchools[$fakeSchoolNumber][0],
        'full_name'  => $fakeSchools[$fakeSchoolNumber][1],
        'city_id' => function() use ($fakeSchools, $fakeSchoolNumber) {
            return \App\Models\City::where('name', $fakeSchools[$fakeSchoolNumber][2])->first()->id ??
                factory(App\Models\City::class)->create()->id;
        },
        'organization_type_id' => function() {
            $organizationType = \App\Models\OrganizationType::first();
            return $organizationType
                ? $organizationType->id
                : factory(\App\Models\OrganizationType::class)->create()->id;
        }
    ];
});
