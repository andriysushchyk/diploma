<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CityType::class, function (Faker $faker) {
    return [
        'title' => $faker->word
    ];
});
