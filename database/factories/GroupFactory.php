<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Group::class, function (Faker $faker) {
    return [
        'year'      => random_int(5, 11),
        'title'     => random_int(5, 11) . "-" . array_random(['A', 'Б', 'В']),
        'school_id' => function () {
            return factory(\App\Models\School::class)->create()->id;
        },
        'year_id'   => \App\Models\Year::current()->id
    ];
});
