<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Mark::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return create(User::class)->id;
        },
        'mark_value_id' => function() {
            return create(\App\Models\MarkValue::class)->id;
        },
        'mark_lesson_type' => function() {
            return (new \App\Models\TimetableItem())->getMorphClass();
        },
        'mark_lesson_id' => function() {
            return create(\App\Models\TimetableItem::class)->id;
        },
        'date' => $faker->date
    ];
});
