<?php

use Faker\Generator as Faker;

$factory->define(App\Models\City::class, function (Faker $faker) {
    return [
        'name'      => $faker->city,
        'type_id'   => function() {
            return factory(\App\Models\CityType::class)->create()->id;
        },
        'region_id' => function () {
            return factory(\App\Models\Region::class)->create()->id;
        },
    ];
});
