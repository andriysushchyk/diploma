<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\NewsItem::class, function (Faker $faker) {
    return [
        'title' => $faker->words(4, true),
        'preview_text' => $faker->text(100),
        'full_text' => $faker->text(1000),
        'image_path' => 'images/default.png',
    ];
});
