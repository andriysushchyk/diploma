<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\TimetableItem::class, function (Faker $faker) {
    return [
        'group_subject_id' => function() {
            return create(\App\Models\GroupSubject::class)->id;
        },
        'day_number' => random_int(1, 5),
        'subject_number' => random_int(1, 7),
    ];
});
