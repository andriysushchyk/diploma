<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\SchoolSettings::class, function (Faker $faker) {
    return [
        'year_id' => \App\Models\Year::current(),
        'school_id' => function() {
            return create(\App\Models\School::class);
        },
        'breaks' => '[]',
        'days_in_week' => array_random([5, 6])
    ];
});
