<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\AdditionalLesson::class, function (Faker $faker) {
    return [
        'date' => $faker->date(),
        'group_subject_id' => function() {
            return factory(\App\Models\GroupSubject::class)->create()->id;
        },
        'lesson_type' => 'potochna'
    ];
});
