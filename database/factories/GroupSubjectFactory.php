<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\GroupSubject::class, function (Faker $faker) {
    return [
        'teacher_id' => function() {
            return create(\App\Models\User::class)->id;
        },
        'group_id' => function() {
            return create(\App\Models\Group::class);
        },
        'title' => 'Математика'
    ];
});
