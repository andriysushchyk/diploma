import Notifications from "./Notifications";

const handleResponseError = (error, customErrorText) => {
    if (error.response && error.response.status == 422) {
        Notifications.validationErrors(error.response.data.errors);
        return;
    }

    Notifications.error(`Упс. Щось пішло не так, як було задумано:<br/>${customErrorText}.<br/>${error}.`);
};

const fillNumber = (numb) =>
{
    if (numb < 10) {
        return "0" + numb;
    }

    return numb;
};


const formatDate = (date) => {
    const dateObj =  new Date(date);
    return fillNumber(dateObj.getDate()) + "." + fillNumber((dateObj.getMonth() + 1));
};

const formatUTC = (date) => {
    const dateObj =  new Date(date);
    return `${dateObj.getUTCFullYear()}-${fillNumber(dateObj.getMonth() + 1)}-${fillNumber(dateObj.getDate())}`;
};


export {handleResponseError, formatDate, formatUTC};

