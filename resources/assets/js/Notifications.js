import Noty from "noty";
import _ from "lodash";

export default {
    success(text) {
        new Noty({
            text:        text,
            type:        "success",
            layout:      "bottomLeft",
            timeout:     1000,
            progressBar: false,
            theme:       "bootstrap-v3"
        }).show();
    },

    validationErrors(errorsFromResponse) {
        const errorsStr = _.reduce(errorsFromResponse, (allErrors, fieldErrors) => {
            return allErrors.concat(fieldErrors);
        }, []).join("<br/>");

        new Noty({
            text: errorsStr,
            type: "warning",
            layout: "bottomLeft",
            timeout: 12000,
            progressBar: false,
            theme: "bootstrap-v3",
            killer: true
        }).show();
    },

    error(text) {
        new Noty({
            text: text,
            type: "error",
            layout: "bottomLeft",
            timeout: 14000,
            progressBar: false,
            theme: "bootstrap-v3",
        }).show();
    }
};
