
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

window.Vue.component("school-application", require("./modules/Guest/SchoolApplication.vue"));
window.Vue.component("auth", require("./modules/Guest/Auth.vue"));

if (document.getElementById("vue-auth-root")) {
    new window.Vue({
        el: "#vue-auth-root"
    });
}

window.Vue.component("timetable", require("./modules/Timetable.vue"));
window.Vue.component("index-teachers", require("./modules/SchoolManagment/IndexTeachers.vue"));
window.Vue.component("index-groups", require("./modules/SchoolManagment/IndexGroups.vue"));
window.Vue.component("index-students", require("./modules/SchoolManagment/IndexStudents.vue"));
window.Vue.component("school-settings", require("./modules/SchoolManagment/SchoolSettings.vue"));
window.Vue.component("index-student-parents", require("./modules/SchoolManagment/IndexStudentParents.vue"));


if (document.getElementById("vue-root")) {
    window.app = new window.Vue({
        el: "#vue-root"
    });
}
