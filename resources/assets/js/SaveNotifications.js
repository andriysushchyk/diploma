import Noty from "noty";

const SavingNotifications = {
    stop() {
        Noty.closeAll("save-notifications");
    },

    save() {
        this.getSavedNotification().show();
    },

    loading() {
        this.getLoadingNotification().show();
    },

    getSavedNotification() {
        return new Noty({
            text: "<i class='fa fa-save'></i> <span class='loading-text'>Зміни успішно збережені</span>",
            type: "success",
            layout: "bottomLeft",
            timeout: 3000,
            progressBar: false,
            theme: "bootstrap-v3",
            queue: "save-notifications",
            killer: "save-notifications"
        });
    },

    getLoadingNotification() {
        return new Noty({
            text: "<i class='fa fa-refresh spinner'></i> <span class='loading-text'>Зміни зберігаються</span>",
            type: "success",
            layout: "bottomLeft",
            timeout: false,
            progressBar: false,
            theme: "bootstrap-v3",
            queue: "save-notifications",
            killer: "save-notifications"
        });
    }

};

export default SavingNotifications;
