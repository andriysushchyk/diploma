const inputMixin = {
    computed: {
        showedLabel() {
            if (this.label) {
                return this.label + ":";
            }

            return this.label;
        }
    },
    props: {
        label: {
            required: false,
            type: String
        },
        id: {
            required: false,
            type: String
        },
        value: {
            required: false,
            default: null
        },

        placeholder: {
            required: false,
            type: String
        },
        help: {
            required: false,
            type: String
        }
    },
    methods: {
        updateValue(value) {
            this.$emit("input", value);
        }
    }
};

export default inputMixin;
