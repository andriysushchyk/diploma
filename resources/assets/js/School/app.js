import wysiwyg from "vue-wysiwyg";
const Vue = require("vue");
Vue.use(wysiwyg, {}); // config is optional. more below

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component("IndexEdMaterials", require("./modules/IndexEdMaterials.vue"));
Vue.component("JournalContainer", require("./modules/JournalContainer.vue"));
Vue.component("DaybookContainer", require("./modules/DaybookContainer.vue"));
Vue.component("ConservationsContainer", require("./modules/ConservationsContainer.vue"));
Vue.component("FullConservationContainer", require("./modules/FullConservationContainer.vue"));
Vue.component("ProfileContainer", require("./modules/ProfileContainer.vue"));
Vue.component("CreateNews", require("./modules/CreateNews.vue"));
Vue.component("NewsContainer", require("./modules/NewsContainer.vue"));
if (document.getElementById("vue-school-root")) {
    new Vue({
        el: "#vue-school-root"
    });
}
