<?php

return [
    'short_name' => 'Коротка назва',
    'full_name' => 'Повна назва',
    'phone_number' => 'Номер телефону',
    'organization_type' => 'Орг. форма',
    'short_name_for_example' => 'Наприклад,Школа № 1',
    'full_name_for_example' => 'Наприклад,Школа № 1',
    'phone_number_for_example' => 'Наприклад,+380 (44) 111-22-22',
];
