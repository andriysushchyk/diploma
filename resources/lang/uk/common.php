<?php

return [
    'first_name'   => "Ім'я",
    'middle_name'  => "Прізвище ",
    'last_name'    => "По-батькові",
    'phone_number' => "Номер телефону",
    'position'     => 'Посада',
    'email'        => 'E-mail',
    'city'         => 'Населений пункт'
];
