<?php

return [
    'short_name' => 'Short name',
    'full_name' => 'Full name',
    'phone_number' => 'Phone number',
    'examples' => [
        'short_name' => 'For example, "School #1"',
        ''
    ]
];
