@extends('school.base')
@section('active-navitem', 'messages')
@section('content')
    <div id="vue-school-root">
        <full-conservation-container
                user-title="{{ $user->first_name . " " . $user->last_name }}"
                user-avatar="{{ $user->avatar_url }}"
                user-name="{{ $user->first_name }}"
                user-id="{{ $user->id }}"
                :messages-prop='@json($messages)'
        ></full-conservation-container>
    </div>
@endsection
