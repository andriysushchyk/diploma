@extends('school.base')
@section('active-navitem', 'messages')
@section('content')
    <div id="vue-school-root">
        <conservations-container :messages='@json($messages)'></conservations-container>
    </div>
@endsection
