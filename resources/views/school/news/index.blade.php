@extends('school.base')
@section('active-navitem', 'news')
@section('content')
    <div class="container box" style="margin-bottom: 50px">
        <div id="vue-school-root">
            <news-container
                    :news='@json($news->items())'
                    next-page-url="{{ $news->nextPageUrl() }}"
                    prev-page-url="{{ $news->previousPageUrl() }}"
                    :can-user-manage-news="{{ \Illuminate\Support\Facades\Auth::user()->canManageNews(
                        $school
                    ) }}"
            ></news-container>
        </div>
    </div>
@endsection
