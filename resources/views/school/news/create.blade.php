@extends('school.base')
@section('active-navitem', 'news')
@section('content')
    <div class="container box">
        <div id="vue-school-root">
            <create-news></create-news>
        </div>
    </div>
@endsection
