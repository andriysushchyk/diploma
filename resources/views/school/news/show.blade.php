@extends('school.base')
@section('active-navitem', 'news')
@section('content')
    <style>
        .image-wrapper {
            display: inline-block;
            width: 130px;
            margin-right: 10px;
        }

        .content {
            width: calc(100% - 150px);
            display: inline-block;
            vertical-align: top;
        }
    </style>
    <div class="container box" style="margin-bottom: 50px">
        <div class="columns">
            <div class="column">
                <div class="image-wrapper">
                    <img src="{{ $newsItem->image_url }}" style="height: 100%; width: 100%;">
                </div>
                <div class="content">
                    <p>
                        <span class="icon">
                        <i class="fa fa-calendar"></i>
                        </span> Опубліковано {{ $newsItem->created_at->format('d.m.y') }} о {{ $newsItem->created_at->format('H:i') }}
                    </p>
                    <h1 style="margin-top: -10px">{{ $newsItem->title }}</h1>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                {!! $newsItem->full_text !!}
            </div>
        </div>
    </div>
@endsection
