@extends('school.base')
@section('active-navitem', 'daybook')
@section('content')
    <div id="vue-school-root">
        @if(\Illuminate\Support\Facades\Auth::user()->role === \App\Models\Role::STUDENT)
            <daybook-container :daybook-data='@json($daybookData)'></daybook-container>
        @else
            <daybook-container :daybook-data='@json($daybookData)' user='@json($user)'></daybook-container>
        @endif
    </div>
@endsection
