@extends('school.base')
@section('content')
    <style>
        @media (max-width: 768px) {
            .title.is-3 {
                margin: auto;
            }

            .user-image {
                margin: auto !important;
                display: block !important;
            }
        }
    </style>
    <div class="container" style="min-height: 300px">
        <div class="columns">
            <div class="column is-8-desktop is-offset-2-desktop is-12-touch">
                <div class="box">
                    <div class="columns">
                        <div class="column">
                            <div class="panel">
                                <div class="panel-heading"
                                     style="margin-right: -21px; margin-left: -21px; margin-top: -21px;">
                                    {{ $user->login }}
                                    <p class="has-text-right" style="float: right"> Профіль</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-2 has-text-right has-text-centered-touch is-vcentered"
                             style="max-height: 200px; line-height: 200px">
                            <img class="user-image" src="{{ $user->avatar_url }}" style="
                    max-height: 200px;
                    min-height: 100px;
                    vertical-align: middle;
                    display: inline-block;
                    border-radius: 4px;
                ;
">
                        </div>
                        <div class="column is-4 has-text-centered-touch"
                             style="height: 200px; align-items: center; display: flex">
                            <h3 class="title is-3">
                                {{ $user->last_name }}<br/>
                                {{ $user->first_name }}<br/>
                                {{ $user->middle_name }}
                            </h3>
                        </div>
                        <div class="column is-6">
                            <table class="table is-fullwidth table is-striped">
                                <tbody>
                                <tr>
                                    <td>Телефон</td>
                                    <td><strong>-</strong></td>
                                </tr>
                                <tr>
                                    <td>E-mail</td>
                                    <td><strong>-</strong></td>
                                </tr>
                                @if($user->role == \App\Models\Role::TEACHER)
                                <tr>
                                    <td>Роль</td>
                                    <td><strong>Вчитель</strong>
                                <tr>
                                    <td>Викладає:</td>
                                    <td>
                                        <ul>
                                            <li><strong>Математика</li>
                                            <li><strong>Фізика</strong></li>
                                        </ul>
                                    </td>
                                </tr>
                                @elseif($user->role == \App\Models\Role::PARENT)
                                    <tr>
                                        <td></td>
                                        <td><strong>Опікун</strong>
                                    <tr>
                                    <tr>
                                        <td>Підопічні</td>
                                        <td>
                                            <ul>
                                                @forelse($user->childs as $child)
                                                    <li><strong>
                                                            <a class="reset-link"
                                                               href="{{ '/school/users/' . $child->login }}">
                                                                {{ $child->first_name }}
                                                            </a>
                                                        </strong></li>
                                                @empty
                                                    -
                                                @endforelse
                                            </ul>
                                        </td>
                                    </tr>
                                @elseif($user->role == \App\Models\Role::STUDENT)
                                <tr>
                                    <td></td>
                                    <td><strong>Учень</strong>
                                <tr>
                                <tr>
                                    <td>Навчальна група:</td>
                                    <td><strong>
                                            {{ $user->groups->first()->title }}
                                        </strong>
                                    </td>
                                </tr>
                                @if(app('request')->user()->role === \App\Models\Role::TEACHER)
                                    <td>Опікуни</td>
                                    <td>
                                        <ul>
                                            @forelse($user->parents as $parent)
                                                <li><strong>
                                                        <a class="reset-link"
                                                           href="{{ '/school/users/' . $parent->login }}">
                                                        {{ $parent->first_name }} {{ $parent->middle_name }}
                                                        </a>
                                                    </strong></li>
                                            @empty
                                                -
                                            @endforelse
                                        </ul>
                                    </td>
                                @endif
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column ">
                            <div class="panel-block" style="display: block; text-align: center">
                                <a class="reset-link" href="{{ url('/school/messages/' . $user->id) }}">
                                <span class="icon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                Надіслати повідомлення
                                </a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
