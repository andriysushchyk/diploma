@extends('school.base')
@section('content')
    <div id="vue-school-root">
        <profile-container :user-data='@json(\Illuminate\Support\Facades\Auth::user())'></profile-container>
    </div>
@endsection
