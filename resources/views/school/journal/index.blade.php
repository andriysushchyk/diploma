@extends('school.base')
@section('content')
    <script>
        window.markValues = @json(\App\Models\MarkValue::all());
    </script>
    <div id="vue-school-root">
        <journal-container
                :users='@json($users)'
                :subject='@json($subject)'
                :lessons-prop='@json($lessonsPaginator->currentPageItems())'
                from-day='{{ $fromDay }}'
                :marks-prop='@json($marks)'
                from-month='{{ $fromMonth }}'
                to-day='{{ $toDay }}'
                to-month='{{ $toMonth }}'
                :lesson-types='@json($lessonTypes)'
                :homeworks-prop='@json($homeworks)'
                prev-page-url='{{
            $lessonsPaginator->prevPageExists()
                ? route('school.journal.show', ['group_subject' => $subject, 'page' => $lessonsPaginator->page() - 1])
                : ''
            }}'
                next-page-url='{{
            $lessonsPaginator->nextPageExists()
                ? route('school.journal.show', ['group_subject' => $subject, 'page' => $lessonsPaginator->page() + 1])
                : ''
            }}'
        ></journal-container>
    </div>
@endsection
