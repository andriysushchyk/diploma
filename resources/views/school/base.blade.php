@php
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;
@endphp
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    <title>Школа</title>
    <style>
        .submenu {
            z-index: 255;
        }

        #@yield('active-navitem') {
            background-color: #118fe4;
            color: #fff;
        }
        body {
            background: whitesmoke;
            padding-bottom: 40px;
            min-height: 100vh;
        }

        @media (max-width: 1024px) {
            body {
                background-color: white;
            }

            .navbar-item--subitem {
                padding-left: 30px;
            }
        }

        @media (min-width: 1024px) {

            .navbar-sublink {
                color: #4a4a4a;
                padding: 0.375rem 1rem;
                white-space: nowrap;
                padding-right: 3rem;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }

            .navbar-sublink:hover {
                background-color: whitesmoke;
                color: #0a0a0a;
            }

            .has-subdropdown {
                padding: 0 !important;
                display: block;
            }

            .navbar-subdropdown {
                left: 100%;
                position: absolute;
                background-color: white;
                border-bottom-left-radius: 5px;
                border-bottom-right-radius: 5px;
                border-top: 1px solid #dbdbdb;
                -webkit-box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1);
                box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1);
                font-size: 0.875rem;
                min-width: 100%;
                z-index: 20;
                transform: translateY(-34px);
                display: none;
            }
        }

    </style>
</head>
<body>
<nav class="navbar is-info" style="margin-bottom: 15px;">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img src="/12345.png" alt="Bulma: a modern CSS framework based on Flexbox"
                     style="width: 130px; height: 35px; max-height: unset">
            </a>
            <div class="navbar-burger burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="navbar-menu" id="navMenu">
            <div class="navbar-start">
                @foreach($menu as $key => $menuItem)
                    @if(isset($menuItem['subitems']))
                        <div class="navbar-item has-dropdown" onclick="openMenu">
                            <a class="navbar-link" data-target="{{ 'navbar-dropdown-' . $key}}">
                            <span class="icon">
                                <i class="{{ $menuItem['icon'] }}"></i>
                            </span>
                                {{ $menuItem['title'] }}
                            </a>
                            <div class="navbar-dropdown" id="{{ 'navbar-dropdown-' . $key}}">
                                @foreach($menuItem['subitems'] as $subkey => $subitem)
                                    @if(isset($subitem['subitems']))
                                        <a class="navbar-item navbar-item--subitem has-subdropdown">
                                            <a class="navbar-sublink"
                                               data-target="{{ 'navbar-subdropdown-' . $subkey}}"
                                               onclick="openSubmenu">{{ $subitem['title'] }}</a>
                                            <span class="icon"
                                                  style="position: absolute; color: black;right: 0; transform: translateY(-29px);"
                                            >
                                                <i class="fa fa-angle-right"></i>
                                            </span>
                                            <div class="navbar-subdropdown" id="{{ 'navbar-subdropdown-' . $subkey}}">
                                                @foreach($subitem['subitems'] as $subsubitem)
                                                    <a href="{{ $subsubitem['link'] }}"
                                                       class="navbar-item">{{ $subsubitem['title'] }}
                                                    </a>
                                                @endforeach
                                            </div>
                                        </a>
                                    @else
                                        <a class="navbar-item navbar-item--subitem"
                                           href="{{ $subitem['link'] }}">
                                            {{ $subitem['title'] }}
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @else
                        <a class="navbar-item" href="{{ $menuItem['link'] }}" id="{{ $key }}">
                    <span class="icon">
                        <i class="{{ $menuItem['icon'] }}"></i>
                    </span>
                            {{ $menuItem['title'] }}
                            @if($menuItem['title'] == 'Повідомлення' && \Illuminate\Support\Facades\Auth::user()->messagesCount())
                                <div style='display: "inline-block";
                                     width: 20px;
                                     height: 20px;
                                     background: red;
                                     -moz-border-radius: 50px;
                                     -webkit-border-radius: 50px;
                                     border-radius: 50px;
                                    margin-left: 5px;
                                    font-size: 14px;
                                    margin-top: -5px;
                                    padding-left: 5px;
                                '
                                > 1</div>
                            @endif
                        </a>
                    @endif
                @endforeach
                <a class="navbar-item is-hidden-desktop" href="{{ route('school.profile.show') }}" id="nav_profile">
                    <span class="icon">
                        <i class="fa fa-user"></i>
                    </span>Профіль
                </a>
                <a class="navbar-item is-hidden-desktop" href="{{ url('/auth/logout') }}" id="nav_logout">
                    <span class="icon">
                        <i class="fa fa-sign-out"></i>
                    </span>Вийти
                </a>
            </div>

            <div class="navbar-end">
                <div class="navbar-item has-dropdown is-hoverable submenu is-hidden-touch">
                    <a class="navbar-link">
                        <span class="icon">
                            <i class="fa fa-user"></i>
                        </span>
                        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                    </a>
                    <div class="navbar-dropdown">
                        @if(Auth::user()->schoolManaged())
                            <a class="navbar-item" href="{{ route('admin.dashboard.index') }}">
                                Панель адміністратора
                            </a>
                        @endif
                        <a class="navbar-item" href="{{ route('school.profile.show') }}">
                            Профіль
                        </a>
                        <a class="navbar-item" href="{{ url('/auth/logout') }}">
                            Вийти
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
@yield('content')
<script src="{{ mix('js/school.js') }}"></script>
<script>
    function hideAllSubdrops(exceptID) {
        var allSubdrops = document.querySelectorAll('.navbar-subdropdown');
        for (var i = 0; i < allSubdrops.length; i++) {
            if (exceptID && allSubdrops[i].id === exceptID) {
                continue;
            }
            allSubdrops[i].style.display = 'none';
        }
    }
    function openMenu(e) {
        console.error('I am opening menu!');
        if (e.target.classList.contains('navbar-sublink')) {
            return false;
        }

        hideAllSubdrops();


        document.getElementById(e.target.dataset.target).style.display =
            (document.getElementById(e.target.dataset.target).style.display === 'block' ? 'none' : 'block');
    }

    function openSubmenu(e) {
        if (e.target.classList.contains('navbar-link')) {
            return false;
        }
        hideAllSubdrops(e.target.dataset.target);


        document.getElementById(e.target.dataset.target).style.display =
            (document.getElementById(e.target.dataset.target).style.display === 'block' ? 'none' : 'block');
    }

    document.addEventListener('DOMContentLoaded', function () {

        // Get all "navbar-burger" elements
        var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
                $el.addEventListener('click', function () {

                    // Get the target from the "data-target" attribute
                    var target = $el.dataset.target;
                    var $target = document.getElementById(target);

                    // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                    $el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        }

        var $dropdownOpeners = Array.prototype.slice.call(document.querySelectorAll('.has-dropdown'), 0);
        $dropdownOpeners.forEach(function ($el) {
            $el.addEventListener('click', openMenu);
        })

        var $dropdownSubOpeners = Array.prototype.slice.call(document.querySelectorAll('.has-subdropdown'), 0);
        $dropdownOpeners.forEach(function ($el) {
            $el.addEventListener('click', openSubmenu);
        })

    });
</script>
</body>
</html>
