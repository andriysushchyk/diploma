@extends('school.base')
@section('content')
    <div id="root">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <div class="box">
                        <div class="panel-heading">
                    <span class="icon">
                        <i class="fa fa-book"></i>
                    </span>
                            Навчальний матеріал
                        </div>
                        <div class="panel-block">
                            <h2 class="title is-2">
                                {{ $educationMaterial->title }}
                            </h2>
                        </div>
                        <div class="panel-block">
                            <p>{{ $educationMaterial->description }}</p>
                        </div>
                        @if($educationMaterial->path_to_file)
                            <div class="panel-block">
                            <a class="reset-link" href="/school/ed_materials/{{ $educationMaterial->id }}/download/">
                                <span class="icon">
                                    <i class="fa fa-download"></i>
                                </span>
                                Завантажити <i>{{ $educationMaterial->filename }}</i>
                            </a>
                            </div>
                        @endif
                    </div>
                </div>

                </div>
            </div>
            <div class="panel">


        </div>
    </div>
@endsection
