@extends('school.base')
@section('content')
    <div id="vue-school-root">
        <index-ed-materials :subject='@json($groupSubject)'
                            :can-edit="{{ \Illuminate\Support\Facades\Auth::user()->role == \App\Models\Role::TEACHER }}"
                            :materials-prop='@json($materials)'
        >

        </index-ed-materials>
    </div>
@endsection
