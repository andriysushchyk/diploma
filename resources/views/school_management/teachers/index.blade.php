@extends('school_management.base')
@section('subtitle', 'Керування списком вчителів')
@section('content')
    <div id="vue-root">
        <index-teachers :teachers='@json($teachers)'></index-teachers>
    </div>
@endsection
