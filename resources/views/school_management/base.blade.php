@php
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;
@endphp
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    <title>Панель адміністратора</title>
    <style>
        .menu-list a:hover:not(.is-active) {
            background: hsl(0, 0%, 80%);
        }

        .menu-list li a {
            min-width: 100px;
        }

        .nested-menu-item ul {
            border-left: none !important;
        }

        .custom-navmenu > .menu-list > li > a {
            min-width: 200px;
        }

        .custom-navmenu {
            position: absolute;
            background: whitesmoke;
            z-index: 999;
            padding: 20px;
            top: 70px;
            display: none;
        }

        .custom-navmenu__item-has-dropdown {
            position: relative;
        }

        .custom-navmenu__item-has-dropdown > .icon {
            position: absolute;
            right: 0;
            top: 5px
        }

        .custom-navmenu__nested-menu {
            position: absolute;
            top: 0;
            left: calc(100% + 21px);
            background: whitesmoke
        }

        .custom-navmenu__nested-menu[data-nesting-level="2"] {
            left: calc(100% + 13px);
        }

        .top-panel {
            background: hsl(0, 0%, 96%);
            margin-top: 10px;
            border-radius: 10px;
        }

        .custom-navmenu__nested-menu {
            display: none;
        }

        .custom-navmenu__nested-menu .menu-list {
            border-left: none !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="columns top-panel">
        <div class="column is-3">
            <button class="button" id="menu_button">
                <span class="icon"><i class="fa fa-align-justify"></i></span>
                <span>Mеню</span>
            </button>
        </div>
        <div class="column is-6 has-text-centered">
            <h3 class="title is-4">Панель адміністратора</h3>
            <h3 class="subtitle is-4">@yield('subtitle')</h3>
        </div>
        <div class="column is-3 has-text-right">
            <a class="button" href="{{ url('/school') }}">
                <span class="icon"><i class="fa fa-home"></i></span>
                <span>{{ Auth::user()->schoolManaged()->short_name }}</span>
            </a>
        </div>
    </div>
    <aside class="menu custom-navmenu" id="custom-navmenu">
        <ul class="menu-list">
            <li>
                <a @if(Route::currentRouteName() == 'admin.dashboard.index') class="is-active" @endif
                href="{{ route('admin.dashboard.index') }}">
                    Головна панель
                </a>
            </li>
            <li>
                <a @if(Route::currentRouteName() == 'admin.settings.index') class="is-active" @endif
                href="{{ route('admin.settings.index') }}">
                    Налаштування
                </a>
            </li>
            <li>
                <a
                        @if(Route::currentRouteName() == 'teachers.index') class="is-active" @endif
                href="{{ route('teachers.index') }}">
                    Вчителі
                </a>
            </li>
            <li>
                <a @if(Route::currentRouteName() == 'admin.groups.index') class="is-active" @endif
                    href="{{ route('admin.groups.index') }}">
                    Навчальні групи
                </a>
            </li>
            <li class="custom-navmenu__item-has-dropdown">
                <a class="dropdown-button @if(Route::currentRouteName() == 'students.index') is-active @endif">Учні</a>
                <span class="icon">
                     <i class="fa fa-angle-right"></i>
                 </span>
                @include('school_management.components.groups_nested_list', [
                    'routename' => 'students.index',
                    'groups' => $groupsInNavMenu
                ])
            <li class="custom-navmenu__item-has-dropdown">
                <a class="dropdown-button @if(Route::currentRouteName() == 'timetable.index') is-active @endif">
                    Розклад уроків
                </a>
                <span class="icon">
                    <i class="fa fa-angle-right"></i>
                </span>
                @include('school_management.components.groups_nested_list', [
                    'routename' => 'timetable.index',
                    'groups' => $groupsInNavMenu
                ])
            </li>
        </ul>
    </aside>
</div>
<div class="container container--menu" style="display: none">

</div>
<br/>
@yield('content')
<script src="{{ mix('/js/app.js') }}"></script>
<script>

    function closeNavMenuByBodyClick(event) {
        if (document.getElementById('menu_button') === event.target
            || document.getElementById('custom-navmenu').contains(event.target)
            || document.getElementById('menu_button').contains(event.target)
        ) {
            return;
        }

        document.getElementById('custom-navmenu').style.display = 'none';
    }

    function toggleNavBarMenu() {
        var menu = document.getElementById('custom-navmenu');
        menu.style.display = menu.style.display == 'block' ? 'none' : 'block';
    }

    function toggleDropdown(e) {
        window.obj = e.target;
        var childrens = e.target.parentNode.children;
        var nestedDropdownMenu = childrens[childrens.length - 1];

        var nestedMenus = document.getElementsByClassName('custom-navmenu__nested-menu');
        var nestingLevel = nestedDropdownMenu.getAttribute('data-nesting-level');
        for (var i = 0; i < nestedMenus.length; i++) {
            if (nestingLevel == 1 && nestedMenus[i] !== nestedDropdownMenu) {
                nestedMenus[i].style.display = 'none';
            }

            if (nestingLevel == 2 && nestedMenus[i].getAttribute('data-nesting-level') == 2) {
                nestedMenus[i].style.display = 'none';
            }
        }

        nestedDropdownMenu.style.display = nestedDropdownMenu.style.display === 'block' ? 'none' : 'block';

    }

    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById('menu_button').addEventListener('click', toggleNavBarMenu);
        document.addEventListener('click', closeNavMenuByBodyClick);
        var toggleButtons = document.getElementsByClassName('dropdown-button');
        for (var i = 0; i < toggleButtons.length; i++) {
            toggleButtons[i].addEventListener('click', toggleDropdown);

        }

    });
</script>
</body>
</html>
