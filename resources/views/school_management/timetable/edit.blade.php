@extends('school_management.base')
@section('subtitle', 'Керування розкладом уроків для навчальної групи `' . $group->title .'`')
@section('content')
<div id="vue-root">
    <timetable :subjects='@json($subjects)'
               :teachers='@json($teachers)'
               :days-count="{{ $group->school->getSettingsForCurrentYear()->days_in_week }}"
               :daily-subjects-count="7"
               :group-id="{{ $group->id }}"
               :group-timetable="{{ $timetable }}"
    ></timetable>
</div>
@endsection
