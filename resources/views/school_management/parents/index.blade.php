@extends('school_management.base')
@section('subtitle', 'Керування опікунами для учня ' . "{$user->first_name} {$user->last_name}")
@section('content')
    <div id="vue-root">
        <index-student-parents
            :parents='@json($user->parents)'
            :user-id="{{ $user->id }}"
        ></index-student-parents>
    </div>
@endsection
