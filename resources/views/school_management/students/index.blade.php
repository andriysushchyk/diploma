@extends('school_management.base')
@section('subtitle', "Керування списком учнів навчальної групи `{$group->title}`")
@section('content')
    <div id="vue-root">
        <index-students :group-id='{{ $group->id }}' :students='@json($students)'></index-students>
    </div>
@endsection
