@extends('school_management.base')
@section('subtitle', 'Керування навчальними групами')
@section('content')
    <div id="vue-root">
        <index-groups :groups='@json($groups)'></index-groups>
    </div>
@endsection
