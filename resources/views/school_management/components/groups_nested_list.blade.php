<div class="menu custom-navmenu__nested-menu" data-nesting-level="1">
    <ul class="menu-list">
        @foreach($groups as $year => $groupsForThisYear)
            @if(count($groupsForThisYear) == 1)
                <li>
                    <a
                            href="{{ route($routename, [$groupsForThisYear[0]['id']]) }}">
                        {{ $groupsForThisYear[0]['title'] }}
                    </a>
                </li>
            @elseif(count($groupsForThisYear) > 1)
                <li class="custom-navmenu__item-has-dropdown">
                    <a class="dropdown-button">{{ $year }}</a>
                    <span class="icon">
                                                <i class="fa fa-angle-right"></i>
                                            </span>
                    <div class="menu custom-navmenu__nested-menu" data-nesting-level="2">
                        <ul class="menu-list">
                            @foreach($groupsForThisYear as $groupForThisYear)
                                <li>
                                    <a href="{{ route($routename, [$groupForThisYear['id']]) }}">
                                        {{ $groupForThisYear['title'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endif
        @endforeach
    </ul>
</div>
