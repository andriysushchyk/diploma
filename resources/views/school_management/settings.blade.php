@extends('school_management.base')
@section('subtitle', 'Керуваннями налаштуваннями')
@section('content')
    <div id="vue-root">
        <school-settings
                :breaks='@json($settings->breaks)'
                :days-in-week='@json($settings->days_in_week)'
        ></school-settings>
    </div>
@endsection
