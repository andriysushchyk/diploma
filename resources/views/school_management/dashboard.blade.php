@extends('school_management.base')
@section('subtitle', 'Головна панель')
@section('content')
    <div class="container">
        <div class="columns">
            <div class="column is-10 is-offset-1">
                <section class="hero is-info welcome is-small">
                    <div class="hero-body">
                        <div class="container">
                            <h1 class="title">
                                Вітаємо!
                            </h1>
                            <h2 class="subtitle">
                                Ви знаходитесь на панелі адміністратора для навчального закладу <br/>
                                `{{ \Illuminate\Support\Facades\Auth::user()->schoolManaged()->full_name }}`
                            </h2>
                        </div>
                    </div>
                </section>
                <section class="info-tiles">
                    <div class="tile is-ancestor has-text-left">
                        <div class="tile is-parent">
                            <article class="tile is-child box content">
                                @if($school->getSettingsForCurrentYear()->is_active)
                                    <p class="has-text-centered">
                                        Ваша школа активована на навчальний заклад на навчальний рік 2018/2019.
                                    </p>
                                    <div class="has-text-centered">
                                        <a href="/school_management/download" class="has-text-centered">
                                        <span class="icon">
                                            <i class="fa fa-download"></i>
                                        </span>
                                            Завантажити список користувачів
                                        </a>
                                    </div>
                                @elseif($notPassedRules->isEmpty())
                                    <p class="subtitle has-text-centered">
                                        Ви можете активувати навчальний заклад на навчальний рік 2018/2019:
                                    </p>
                                    <form method="post" action="/school_management/activate">
                                        {{ csrf_field() }}
                                        <p class="has-text-centered">
                                            <button class="button is-info">Активувати</button>
                                        </p>
                                    </form>
                                @else
                                <p class="subtitle">
                                    Для того щоб активувати навчальний заклад для навчального року 2018/2019 необхідно:
                                </p>
                                <ul>
                                    @foreach($notPassedRules as $notPassedRule)
                                    <li><a href="{{ $notPassedRule->getLink() }}">{{ $notPassedRule->getNotPassedText() }}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </article>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
