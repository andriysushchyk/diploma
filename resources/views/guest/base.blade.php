
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <style>
        .navbar-menu-container {
            max-width: 1000px;
            margin: auto;
            /*background: #0b2e13;*/
        }

        .logo-img {
            max-width: 100%;
            height: 100px;
            display: block;
            width: auto;
            margin: auto;
        }

        .custom-navbar__item {
            display: inline-block;
            width: 32%;
            line-height: 50px;
            height: 50px;
            vertical-align: middle;
            text-align: center;
        }

        .custom-navbar__item > a, .custom-navbar__item > a:hover, .custom-navbar__item > a:visited {
            color: white;
            height: 100%;
            line-height: 100%;
        }

        .custom-navbar {
            margin-top: 25px;
            width: 100%;
            background: #0c5460;
            height: 50px;
            border-radius: 15px;
        }

    </style>
</head>
<body>
<div class="container navbar-menu-container">
    <div class="columns">
        <div class="column is-6">
            <a href="{{ url('/') }}">
                <img class="logo-img" src="http://www.lorem-ipsum.nl/wp-content/uploads/2017/07/logo-1.png">
            </a>
        </div>
        <div class="column is-6">
            <div class="custom-navbar">
                <div class="custom-navbar__item">
                    <a href="{{ url('/about') }}">Про проект</a>
                </div>
                <div class="custom-navbar__item">
                    <a href="{{ url('/join') }}">Приєднатись</a>
                </div>
                <div class="custom-navbar__item">
                    <a id="_auth" href="#">Увійти</a>
                </div>
            </div>
        </div>
    </div>
    <div id="vue-auth-root">
        <school-application></school-application>
    </div>
</div>
@yield('content')
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
