<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Школа - головна сторінка</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Bulma Version 0.7.1-->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<section class="hero is-info is-medium is-bold">
    <div class="hero-head">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand" style="height: 65px; width: 180px">
                    <img src="{{ url('/12345.png') }}">
                </div>
                <div id="navbarMenu" class="navbar-menu">
                    <div class="navbar-end">
                        <a class="navbar-item" id="_join">
                            Приєднатись
                        </a>
                        <a class="navbar-item" id="_auth">
                            Увійти
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="hero-body">
        <div class="container has-text-centered">
            <h1 class="title">
                Всеукраїнська безкоштовна шкільна освітня мережа
            </h1>
            <h2 class="subtitle">
                Всеукраїнська безкоштовна освітня мережа «Школа» формує унікальне електронне середовище для вчителів,
                учнів та їх батьків.<br/>
                Метою проекту є об'єднання всіх педагогів, учнів та батьків України в єдину спільноту, модернізація
                навчального процесу та впровадження сучасних комп'ютерних технологій у школах.
            </h2>
        </div>
    </div>

</section>

<div class="box cta">
    <p class="has-text-centered"></p>
</div>

<section class="container">
    @foreach($features->chunk(3) as $threeFeatures)
        <div class="columns features">
            @foreach($threeFeatures as $feature)
                <div class="column is-4">
                    <div class="card is-shady">
                        <div class="card-image has-text-centered">
                            <i class="fa fa-{{ $feature['fa-class'] }} fa-3x"></i>
                        </div>
                        <div class="card-content">
                            <div class="content">
                                <h4 class="has-text-centered">{{ $feature['title'] }}</h4>
                                <p>{!!  $feature['def']  !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</section>
<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <p>
                Дипломний проект студента КП-42 <strong>Сущика Андрія</strong>.<br/>
                Вихідний код доступний за <a href="github.com/sushchyk">посиланням</a>
            </p>
        </div>
    </div>
</footer>
<div id="vue-auth-root">
    <auth></auth>
    <school-application
            :regions='@json($regions)'
            :cities='@json($cities)'
            :organization-types='@json($types)'
    ></school-application>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
