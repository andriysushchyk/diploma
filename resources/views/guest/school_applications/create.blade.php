@extends('guest.base')
@section('content')
    <div id="vue-root">
        <school-application
                :regions='@json($regions)'
                :cities='@json($cities)'
                :organization-types='@json($types)'
        ></school-application>
    </div>
@endsection
